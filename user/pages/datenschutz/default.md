---
title: "Datenschutz"
description: "Datenschutz"
published: true
---

## Datenschutzerklärung
 
**1. Name und Kontaktdaten des für die Verarbeitung Verantwortlichen sowie des betrieblichen Datenschutzbeauftragten**  
Diese Datenschutzinformation gilt für die Datenverarbeitung durch  
foodsharing e. V.  
Neven-DuMont-Str. 14  
50667 Köln  
und für folgende Internetseiten (inkl. Subdomains):  
foodsharing-staedte.org  

Der betriebliche Datenschutzbeauftragte ist unter der o. g. Anschrift, z. Hd. Abteilung Datenschutz, bzw. per E-Mail unter datenschutz@foodsharing.de erreichbar.

**2. Erhebung und Speicherung personenbezogener Daten sowie Art und Zweck von deren Verwendung**  
a) Beim Besuch der Website  
Beim Aufruf der o. g. Websites werden durch den auf deinem Endgerät zum Einsatz kommenden Browser automatisch Informationen an den Server unserer Website gesendet. Diese Informationen werden temporär in einem sog. Logfile gespeichert. Folgende Informationen werden dabei ohne dein Zutun erfasst und bis zur automatisierten Löschung gespeichert:  
- IP-Adresse des anfragenden Rechners,
- Datum und Uhrzeit des Zugriffs,
- Name und URL der abgerufenen Datei,
- Website, von der aus der Zugriff erfolgt (Referrer-URL),
- verwendeter Browser und ggf. das Betriebssystem deines Rechners sowie
- der Name deines Zugangsanbieters.  
Die genannten Daten werden durch uns zu folgenden Zwecken verarbeitet:  
- Gewährleistung eines reibungslosen Verbindungsaufbaus der Website,
- Gewährleistung einer komfortablen Nutzung unserer Website,
- Auswertung der Systemsicherheit und -stabilität sowie
- zu weiteren administrativen Zwecken.

Die Rechtsgrundlage für die Datenverarbeitung ist Art. 6 Abs. 1 S. 1 lit. f DSGVO. Unser berechtigtes Interesse folgt aus oben aufgelisteten Zwecken zur Datenerhebung. In keinem Fall verwenden wir die erhobenen Daten zu dem Zweck, Rückschlüsse auf deine Person zu ziehen.
Darüber hinaus setzen wir beim Besuch unserer Website Cookies sowie Analysedienste ein. Nähere Erläuterungen dazu erhältst du unter den Ziffern 4 und 5 dieser Datenschutzerklärung.
b) Bei Anmeldung für unseren Newsletter  
Sofern du nach Art. 6 Abs. 1 S. 1 lit. a DSGVO ausdrücklich eingewilligt hast, verwenden wir deine E-Mail-Adresse dafür, dir regelmäßig unseren Newsletter zu übersenden. Für den Empfang des Newsletters ist die Angabe einer E-Mail-Adresse ausreichend.
Die Abmeldung ist jederzeit möglich, zum Beispiel über einen Link am Ende eines jeden Newsletters. Alternativ kannst du deinen Abmeldewunsch gerne auch jederzeit per E-Mail an kontakt@foodsharing-staedte.org senden.
c) Bei Nutzung unseres Kontaktformulars oder anderer Kontaktmöglichkeiten
Wenn du uns per E-Mail oder über andere Kontaktmöglichkeiten anschreibst, willigst du der Verarbeitung der auf diesem Weg übermittelten Daten gemäß Art. 6 Abs. 1 S. 1 lit. a DSGVO ein. Für die Weitergabe dieser Daten gelten die in Ziffer 3 getroffenen Ausführungen.

**3. Weitergabe von Daten**  
Eine Übermittlung deiner persönlichen Daten an Dritte zu anderen als den im Folgenden aufgeführten Zwecken findet nicht statt.
Wir geben deine persönlichen Daten nur an Dritte weiter, wenn:
- Du deine nach Art. 6 Abs. 1 S. 1 lit. a DSGVO ausdrückliche Einwilligung dazu erteilt hast,
- die Weitergabe nach Art. 6 Abs. 1 S. 1 lit. f DSGVO zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist und kein Grund zur Annahme besteht, dass du ein überwiegendes schutzwürdiges Interesse an der Nichtweitergabe deiner Daten hast,
- für den Fall, dass für die Weitergabe nach Art. 6 Abs. 1 S. 1 lit. c DSGVO eine gesetzliche Verpflichtung besteht, sowie
- dies gesetzlich zulässig und nach Art. 6 Abs. 1 S. 1 lit. b DSGVO für die Abwicklung von Vertragsverhältnissen mit dir erforderlich ist (bspw. die Übermittlung von Name und Adresse an die Post bzw. den Paketzusteller, wenn du Infomaterial bei uns bestellst).

**4. Aktive Inhalte und Cookies**  
a) JavaScript  
JavaScript ist eine clientseitige Skriptsprache, die verwendet wird, um Benutzerinteraktion durchzuführen bzw. auszuwerten, Seiteninhalte zu verändern, nachzuladen oder zu generieren. Foodsharing.de verwendet JavaScript zu ebendiesen Zwecken. Die Übertragung von Daten zwischen deinem Browser und der Anwendung erfolgt dabei verschlüsselt (vgl. Ziff. 9). Die meisten Browser akzeptieren JavaScript automatisch. Du kannst deinen Browser jedoch so konfigurieren, dass keine aktiven Inhalte auf deinem Computer ausgeführt werden. Die vollständige Deaktivierung von JavaScript wird mitunter dazu führen, dass du nicht alle Funktionen der Seite wie vorgesehen nutzen kannst.

b) Cookies  
Wir setzen auf unserer Seite Cookies ein. Hierbei handelt es sich um kleine Dateien, die dein Browser automatisch erstellt und die auf deinem Endgerät (Laptop, Tablet, Smartphone o. ä.) gespeichert werden, wenn du unsere Seite besuchst. Cookies richten auf deinem Endgerät keinen Schaden an, enthalten keine Viren, Trojaner oder sonstige Schadsoftware.
In dem Cookie werden Informationen abgelegt, die sich jeweils im Zusammenhang mit dem spezifisch eingesetzten Endgerät ergeben. Damit können wir erkennen, wenn Benutzer im Rahmen einer Sitzung oder auch später erneut Seiten unserer Plattform aufrufen. Dies bedeutet, dass wir Benutzer über die Dauer ihres Besuchs wiedererkennen können, jedoch nicht, dass wir dadurch unmittelbar Kenntnis von deren Identität erhalten. 
Der Einsatz von Cookies dient einerseits dazu, die Nutzung unseres Angebots für dich angenehmer zu gestalten. So setzen wir sogenannte Session-Cookies zur Ablaufsteuerung und der Übertragung eingegebener Daten an Folgeseiten ein. Diese werden gelöscht, wenn du dich von der Seite abmeldest oder deinen Webbrowser schließt.  
Darüber hinaus setzen wir ebenfalls zur Optimierung der Benutzerfreundlichkeit temporäre Cookies ein, die für einen bestimmten festgelegten Zeitraum auf deinem Endgerät gespeichert werden. Besuchst du unsere Seite erneut, um unsere Dienste in Anspruch zu nehmen, wird automatisch erkannt, dass du bereits bei uns warst und welche Eingaben und Einstellungen du getätigt hast, um diese nicht noch einmal eingeben zu müssen.  
Zum anderen setzen wir Cookies ein, um die Nutzung unserer Website statistisch zu erfassen und zum Zwecke der Optimierung unseres Angebotes für dich auszuwerten (siehe Ziff. 5). Diese Cookies ermöglichen es uns, bei einem erneuten Besuch unserer Seite automatisch zu erkennen, dass du bereits bei uns warst. Diese Cookies werden nach einer jeweils definierten Zeit automatisch gelöscht.  
Die durch Cookies verarbeiteten Daten sind für die genannten Zwecke zur Wahrung unserer berechtigten Interessen sowie der Dritter nach Art. 6 Abs. 1 S. 1 lit. f DSGVO erforderlich.  
Die meisten Browser akzeptieren Cookies automatisch. Du kannst deinen Browser jedoch so konfigurieren, dass keine Cookies auf deinem Computer gespeichert werden oder stets ein Hinweis erscheint, bevor ein neuer Cookie angelegt wird. Die vollständige Deaktivierung von Cookies kann jedoch dazu führen, dass du nicht alle Funktionen unserer Website nutzen kannst.

**5. Analyse-Tools**  
a) Statistische Auswertungen  
Wir nehmen auf Basis der in Ziffer 2a aufgeführten Zugriffsprotokolle statistische Auswertungen auf Grundlage des Art. 6 Abs. 1 S. 1 lit. f DSGVO zum Zwecke der Optimierung unseres Angebots vor. Hierbei werten wir nur auf unserem Server anfallende Daten aus und übertragen keinerlei personenbezogene Zugriffsdaten an Dritte. Wir setzen keinerlei Zählpixel oder Third-Party-Cookies Dritter ein.

b) Facebook Insights  
Mit dem Besuch unserer Facebook-Seite willigst du ein, dass Facebook nutzerbezogene Daten erfasst und uns anonymisiert als Seitenstatistiken (sog. „Insights“) zur Verfügung stellt. Rechtsgrundlage ist damit Art. 6 Abs. 1 S. 1 lit. a resp. b DSGVO. Die Datenerhebung erfolgt in diesem Fall jedoch nicht durch uns, sondern durch Facebook und wir haben auch keinerlei Einfluss auf Art und Umfang der Erhebung, ebenfalls können wir ihr nicht widersprechen. Bei diesbezüglichen Fragen und Auskunftsersuchen wende dich daher bitte direkt an Facebook bzw. an den in deren Datenrichtlinie genannten Datenschutzbeauftragten.

**6. Social Media Plug-ins**  
Wir setzen auf unserer Website derzeit keinerlei Social Media Plug-ins ein.

**7. Betroffenenrechte**    
Du hast das Recht:  
gemäß Art. 15 DSGVO Auskunft über deine von uns verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere kannst du Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen deine Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft deiner Daten, sofern diese nicht bei uns erhoben wurden, sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen;  
gemäß Art. 16 DSGVO unverzüglich die Berichtigung unrichtiger oder Vervollständigung deiner bei uns gespeicherten personenbezogenen Daten zu verlangen;
gemäß Art. 17 DSGVO die Löschung deiner bei uns gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist;  
gemäß Art. 18 DSGVO die Einschränkung der Verarbeitung deiner personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von dir bestritten wird, die Verarbeitung unrechtmäßig ist, du aber deren Löschung ablehnst und wir die Daten nicht mehr benötigen, du diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigst oder du gemäß Art. 21 DSGVO Widerspruch gegen die Verarbeitung eingelegt hast;
gemäß Art. 20 DSGVO deine personenbezogenen Daten, die du uns bereitgestellt hast, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen;  
gemäß Art. 7 Abs. 3 DSGVO deine einmal erteilte Einwilligung jederzeit gegenüber uns zu widerrufen. Dies hat zur Folge, dass wir die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen dürfen, und
dich gemäß Art. 77 DSGVO bei einer Aufsichtsbehörde beschweren, wenn du .der Ansicht bist, dass die Verarbeitung der dich betreffenden personenbezogenen Daten gegen die DSGVO verstößt.
Die für uns unmittelbar zuständige Aufsichtsbehörde ist:  
    Landesbeauftragte für Datenschutz und Informationsfreiheit   
    Nordrhein-Westfalen  
    Postfach 20 04 44  
    40102 Düsseldorf  
Ansonsten kannst du dich aber auch an die Aufsichtsbehörde deines üblichen Aufenthaltsortes oder Arbeitsplatzes wenden.

**8. Widerspruchsrecht**  
Sofern deine personenbezogenen Daten auf Grundlage von berechtigten Interessen gemäß Art. 6 Abs. 1 S. 1 lit. f DSGVO verarbeitet werden, hast du das Recht, gemäß Art. 21 DSGVO Widerspruch gegen die Verarbeitung deiner personenbezogenen Daten einzulegen, soweit dafür Gründe vorliegen, die sich aus deiner besonderen Situation ergeben oder sich der Widerspruch gegen Direktwerbung richtet. Im letzteren Fall hast du ein generelles Widerspruchsrecht, das ohne Angabe einer besonderen Situation von uns umgesetzt wird.
Möchtest du von deinem Widerrufs- oder Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an datenschutz@foodsharing.de

**9. Datensicherheit**  
Wir verwenden innerhalb des Website-Besuchs das verbreitete SSL-Verfahren (Secure Socket Layer) in Verbindung mit der jeweils höchsten Verschlüsselungsstufe, die von deinem Browser unterstützt wird. In der Regel handelt es sich dabei um eine 256-Bit-Verschlüsselung oder höher. Falls dein Browser keine 256-Bit-Verschlüsselung unterstützt, greifen wir stattdessen auf 128-Bit-v3-Technologie zurück. Ob eine einzelne Seite unseres Internetauftrittes verschlüsselt übertragen wird, erkennst du an der geschlossenen Darstellung des Schüssel- beziehungsweise Schloss-Symbols in der Adresszeile oder der Statusleiste deines Browsers.
Wir bedienen uns im Übrigen geeigneter technischer und organisatorischer Sicherheitsmaßnahmen, um deine Daten gegen zufällige oder vorsätzliche Manipulationen, teilweisen oder vollständigen Verlust, Zerstörung oder gegen den unbefugten Zugriff Dritter zu schützen. Unsere Sicherheitsmaßnahmen werden entsprechend der technologischen Entwicklung fortlaufend verbessert.

**10. Aktualität und Änderung dieser Datenschutzerklärung**  
Diese Datenschutzerklärung ist aktuell gültig und hat den Stand Dezember 2019.
Durch die Weiterentwicklung unserer Website und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf unserer Website abgerufen und/oder ausgedruckt werden.
