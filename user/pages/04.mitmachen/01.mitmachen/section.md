---
title: 'Mitmachen'
published: true
visible: false
hide_page_title: true
---

### Jede\*r kann mitmachen! 

Du bist motiviert und hast Lust, bei dir vor der Haustür gegen Lebensmittelverschwendung aktiv zu werden? Dann bist du hier genau richtig.
In unserem [Handbuch (Geräteschuppen)](https://foodsharing-staedte.org/user/pdf/foodsharing-Staedte_Geraeteschuppen.pdf) findest du alle Infos die du brauchst und Antworten auf viele Fragen.
Die Entscheidung zur Teilnahme an der Bewegung wird durch die lokale foodsharing Gruppe und/oder die öffentliche Hand (z. B. öffentliche Verwaltung oder Politiker\*innen) getroffen. Beide Wege sind denkbar und gleichwertig. Im besten Fall ist es ein gemeinsamer Beschluss, da die Ziele durch Zusammenarbeit besser erreicht werden können.
Zum Mitmachen wende dich einfach an: <kontakt@foodsharing-staedte.org> 

Du findest die Idee **foodsharing-Städte** zwar super, möchtest dich aber eigentlich erstmal bei **foodsharing.de** anmelden? [Werde Foodsaver*in!](https://foodsharing.de/?page=content&sub=joininfo)