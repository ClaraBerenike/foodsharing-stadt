---
title: 'Jetzt geht es los'
published: true
visible: false
hide_page_title: true
---

### Jetzt geht es los

Ihr kennt die Ziele der Bewegung und den geplanten Ablauf?! Ihr habt Lust, mitzumachen?! Worauf wartet ihr dann noch? Jetzt könnt ihr mit dem Umsetzen des Ideenkatalogs beginnen. 

Der Ideenkatalog steht im Zentrum der Bewegung und ist unter dem Motto “Der Weg ist das Ziel” zu betrachten. Er zeigt Möglichkeiten auf, sich bei euch vor der Haustür für mehr Lebensmittelwertschätzung stark zu machen.
In diesem Katalog findet ihr Ideen, wie ihr eure Stadt zu einem Ort der Lebensmittelwertschätzung macht. Zwei Ideen sind besonders herausgestellt. Sie sind die Kriterien, die erfüllt sein müssen, um eine foodsharing-Stadt zu werden. 

Manche Ideen sind schnell und leicht umzusetzen, manche erfordern langfristige Planung und wieder andere bauen auf eine gute Zusammenarbeit mit vielen Partner*innen. Jede Stadt ist anders und deshalb könnt ihr frei wählen, welche Ideen ihr wann umsetzen möchtet. 
Die Reihenfolge, die ihr im Ideenkatalog seht, ist unser Vorschlag für eine Stadt, in der bereits der erste Samen gesät ist, aus dem weiteres Engagement wachsen soll. Die ersten sechs Ideen können beinahe alle von der lokalen foodsharing Gruppe ins Leben gerufen werden. Die darauffolgenden sechs Ideen erfordern eine gute Vernetzung von lokaler foodsharing Gruppe und öffentlicher Hand. Die letzten sechs Ideen dienen schließlich dazu, nachhaltige Strukturen für Lebensmittelwertschätzung in eurer Stadt zu etablieren. 

Wir denken, dass Ideen besser umgesetzt werden können, wenn ein engagiertes Team daran arbeitet, das sich der Sache verpflichtet fühlt. Deshalb empfehlen wir, das möglichst zu Beginn in Angriff zu nehmen. 