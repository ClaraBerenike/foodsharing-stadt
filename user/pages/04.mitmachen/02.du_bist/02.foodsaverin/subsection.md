---
title: 'Foodsaver*in'
hide_page_title: true
---

### Foodsaver*in

#### Bei dir vor Ort gibt es bereits eine lokale foodsharing-Gruppe

Optimal – deine Community ist die beste Quelle, um Motivierte für die Umsetzung der Kampagne zu gewinnen! Als erstes könntet ihr z. B. einen informierenden Foren Beitrag erstellen und zu einem ersten Treffen aufrufen. Gemeinsam könnt ihr überlegen, ob ihr euch direkt mit der öffentlichen Hand in Verbindung setzt. Danach könnt ihr zusammen entscheiden, welchen der Punkte im Ideenkatalog ihr zuerst angehen möchtet. Vergesst nicht, das Organisationsteam der Bewegung foodsharing-Städte zu kontaktieren. So wird eure Teilnahme transparent und ihr habt direkt eine Idee erfüllt, da ihr dann offiziell Teil der Bewegung seid. 

<p class="prev-next">

<a class="btn btn-primary" href=/de/mitmachen/du_bist>
                            <i class="fa fa-chevron-left"></i>
                            zurück zur Ausgangssituation</a>

</p>