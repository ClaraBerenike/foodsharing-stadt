---
title: 'Weltretter*in'
hide_page_title: true
---

### Weltretter\*in
#### Es gibt bereits eine politisch sehr aktive lokale foodsharing Gruppe, die bestens mit der öffentlichen Hand vernetzt ist. Lebensmittelverschwendung ist in deiner Stadt ein Fremdwort:
Selbst dann bist du hier richtig! Ihr setzt vor Ort bereits alles um, was wir uns für alle Städte wünschen würden. Wir würden uns deshalb unglaublich freuen, wenn ihr im Rahmen der Bewegung eure Erfahrungen mit anderen Städten teilt. 

<p class="prev-next">

<a class="btn btn-primary" href=/de/mitmachen/du_bist>
                            <i class="fa fa-chevron-left"></i>
                            zurück zur Ausgangssituation</a>

</p>