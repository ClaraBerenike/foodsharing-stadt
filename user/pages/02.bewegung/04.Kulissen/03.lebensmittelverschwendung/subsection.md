---
title: 'Problem'
hide_page_title: true
---

### Wo liegt das Problem?

Insgesamt landen in Deutschland pro Jahr fast 13 Millionen Tonnen Lebensmittel im Müll. Die Konsequenzen davon sind nicht nur hohe Abfallmengen. Der so sorglose Umgang mit Lebensmitteln hat darüber hinaus auch drastische Auswirkungen auf den CO2-Ausstoß und somit direkten Einfluss auf die Klimakrise. Es entstehen unnötig Emissionen bei der Produktion, während der Logistik und bei der Lagerung von Waren, die dann am Ende in der Tonne landen.