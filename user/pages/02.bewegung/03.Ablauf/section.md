---
title: 'Ablauf der Kampagne'
published: true
visible: false
hide_page_title: true
---

### Ablauf der Kampagne

<style>
h4 {
border-width: 2px;
border-style: solid;
}
</style>


#### Schritt 1
Mit dem ersten Schritt sät ihr den Samen für euren Weg zu einer Stadt der Lebensmittelwertschätzung: Bildet eine Gruppe, die sich dem Ziel verpflichtet fühlt und nehmt Kontakt zum Organisationsteam auf. 

Jetzt erhaltet ihr ein Profil auf unserer Webseite mit einem eigenen Baum. Dieser symbolisiert euren Einsatz für Lebensmittelwertschätzung. Eure Gruppe bildet die Wurzel eures Engagements. Sie ist außerdem das erste Kriterium für die Auszeichnung als foodsharing-Stadt.

#### Schritt 2
Um Lebensmittelverschwendung nachhaltig zu bekämpfen, braucht es die Zusammenarbeit von Zivilgesellschaft und Politik. Die gemeinsame Verpflichtung der lokalen foodsharing-Gruppe und der öffentlichen Hand für mehr Lebensmittelwertschätzung zu sorgen, schafft einen stabilen Stamm für eure Arbeit. Die Motivationserklärung dient als Symbol und Vertrag dieser Partnerschaft. Schickt sie unterschrieben an das Organisationsteam und erfüllt so das zweite Kriterium zur foodsharing-Stadt. Danach könnt ihr das auf eure Stadt angepasste Logo der Bewegung nutzen.

Im Ideenkatalog findet ihr Vorschläge für Aktionen. Überlegt gemeinsam, welche Punkte ihr schon umgesetzt habt und welche Ideen ihr als nächstes umsetzen wollt. Wertvolle Tipps und Tricks, wie du und dein Team möglichst viele Ideen umsetzen könnt und ihr foodsharing-Stadt werdet, findet ihr im Geräteschuppen, unserem Handbuch zur Gestaltung einer foodsharing-Stadt. Je mehr Ideen ihr umsetzt, desto mehr Früchte wird euer Baum tragen. 

#### Schritt 3
Ihr seid kurz davor, eine oder mehrere Ideen Realität werden zu lassen? Super, wir sind genauso gespannt wie ihr! Wir wollen unsere Erfahrungen miteinander teilen, damit wir gegenseitig davon lernen und profitieren können. Deshalb gibt es zu jeder Idee eine kurze Anleitung wie ihr sie dokumentiert.

#### Schritt 4
Schickt die jeweilige Dokumentation eurer Idee an das Organisationsteam. Kurze Zeit später wird dies auf eurem Profil auf unserer Webseite sichtbar: eine neue Frucht erscheint an eurem Baum. 

Euer Baum hängt voller Früchte und ihr habt alle Ideen des Katalogs in eurer Stadt implementiert? Zeigt weiterhin euer Engagement für Lebensmittelwertschätzung, teilt uns eure Ideen mit und helft  anderen Städten, ebenfalls alle Punkte des Ideenkatalogs umzusetzen. 

**Viel Spaß und viel Erfolg bei eurer Arbeit für mehr Lebensmittelwertschätzung in euer Stadt wünscht euch das Organisationsteam foodsharing-Städte**


 
