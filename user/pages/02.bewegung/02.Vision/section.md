---
title: 'Unsere Vision'
published: true
visible: false
hide_page_title: true
---

### Unsere Vision


Stell dir vor, du lebst in einer Stadt, in der es ausreichend Lebensmittel für alle gibt. Genug, damit jede\*r leckeres Essen genießen kann. Häufig finden Volksküchen statt, bei denen eine Gruppe für viele Menschen kocht. Alles, was nicht sofort gegessen wird, machen die Bewohner\*innen gemeinschaftlich haltbar. 

Es herrscht eine Kultur des Tauschens und Teilens – wer von etwas zu viel hat, gibt es an Freund\*innen, Bekannte oder Nachbar\*innen weiter. Alternativ kann es natürlich auch in einen der vielen Fairteiler gebracht werden, die an öffentlichen Orten stehen. 

Die meisten Lebensmittel werden regional angebaut und sind der Saison entsprechend immer frisch. Viele Menschen haben sich zu solidarischen Landwirtschaften zusammengeschlossen und es gibt food-Kooperativen, die viele biologisch angebaute und faire Produkte verkaufen. 

Damit es ein ganzheitliches Konzept für die Versorgungssicherheit in der Stadt gibt, treffen sich Vertreter*innen aus Politik, Landwirtschaft, Handel, Außer-Haus-Verpflegung und Zivilgesellschaft regelmäßig zu einem runden Tisch. Dort wird gemeinsam überlegt und entschieden, wie mit dem Thema Ernährung möglichst nachhaltig im Alltag Aller umgegangen werden kann und wie Menschen für mehr Lebensmittelwertschätzung sensibilisiert werden können.