---
title: 'Pressemitteilung zum Start der Bewegung'
date: '12:00 12/06/2019'
taxonomy:
    category:
        - blog
first_image: 2019-12-06_PM/logo.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: 'Foodsharing-Städte: Gemeinsam für Lebensmittelwertschätzung'
---

Wie sich foodsharing künftig gemeinsam mit Einwohner*innen und Stadtverwaltung gegen Lebensmittelverschwendung stark macht

Insgesamt landen in Deutschland pro Jahr fast 13 Millionen Tonnen Lebensmittel im Müll. Die Konsequenzen davon sind nicht nur hohe Abfallmengen. Der so sorglose Umgang mit Lebensmitteln hat darüber hinaus auch drastische Auswirkungen auf den CO2-Ausstoß und somit direkten Einfluss auf die Klimakrise. Emissionen, die bei der Produktion, während der Logistik und bei der Lagerung angefallen sind, landen zusammen mit den Waren selbst in der Tonne.

Die Bewegung foodsharing-Städte will als Teil der foodsharing-Organisation künftig zusätzliche Wege gehen: „Wir wollen abseits des Lebensmittelrettens darauf aufmerksam machen, wie wichtig Lebensmittelwertschätzung für das Klima und den Lebenswert in unseren Städten ist“, sagen Vertreter\*innen des Organisationsteams der Bewegung. „Ziel der Bewegung ist es, das Thema Lebensmittelwertschätzung in den Alltag der Menschen zu integrieren und lokales Engagement zu fördern. Als Teil einer deutschlandweiten Bewegung soll jede Stadt angeregt werden, auf ihre Weise ein Konzept zur Wertschätzung von Lebensmitteln umzusetzen. Orte, an denen das gelingt, werden – ebenso wie die Initiative selbst – „foodsharing-Städte“ genannt“, erklärt das Team weiter. Ganz nach dem Motto gemeinsam stark will die Bewegung dies durch die Kooperationen zwischen aktiven Foodsaver\*innen, der öffentlichen Verwaltung und anderen Organisationen, wie beispielsweise Tafeln, erreichen. Am 12.12.2019 – pünktlich zum siebten Geburtstag der foodsharing-Organisation – findet der offizielle Bewegungsstart sowie das Go-Live der Webseite www.foodsharing-staedte.org statt. 

Alle Kooperationspartner\*innen arbeiten gemeinsam an der Umsetzung eines Ideenkatalogs. Dieser beinhaltet 18 Ideen, um Lebensmittelwertschätzung in der Stadt zu steigern. Der Ideenkatalog verbindet teilnehmende Städte deutschlandweit und bietet Anregungen für eine lebenswertere Stadt. Gleichzeitig ermöglicht der Katalog jeder Stadt, Nachhaltigkeit individuell und lokal zu gestalten. Informationen über die Einrichtung sogenannter „Fairteiler“ (Orte, an denen Lebensmittel geteilt werden können), die Einbindung von Lebensmittelverschwendung in die städtische Kommunikation sowie die Initiierung von Bildungsangeboten in Schulen, sind nur einige wenige Beispiele aus diesem Katalog.
Zusätzlich wird die Anerkennung “foodsharing-Stadt” an die Städte vergeben, in denen ein aktives Team an der Umsetzung dieser Ideen arbeitet und dies in Form einer schriftlichen Vereinbarung zwischen den Partner*innen festgehalten wurde. Anerkannte foodsharing-Städte erhalten ein individualisiertes Logo, welches sie zur Gestaltung Ihrer Arbeit nutzen können.

Alle teilnehmenden Städte erhalten darüber hinaus einen eigenen Bereich auf www.foodsharing-staedte.org, in dem sie über lokale Aktivitäten und Events informieren können. Weiterhin stehen auf der Webseite Materialien rund um das Thema Engagement für Lebensmittelwertschätzung und Hilfestellungen für Teilnehmende und Interessierte bereit. Die Webseite dient somit als Plattform des gegenseitigen Austauschs, honoriert lokale Anstrengungen gegen Lebensmittelverschwendung und macht sie sichtbar.

Bereits zum Bewegungsstart am 12.12.2019 werden sich mehrere Pilotstädte – darunter Wien, Stuttgart, Braunschweig und Darmstadt – an der Bewegung beteiligten und so als Vorreiter im Kampf gegen Lebensmittelverschwendung mit gutem Beispiel vorangehen. Eine ganze Woche lang feiert die Bewegung ihren Geburtstag und stellt täglich weitere Teilnehmerstädte auf www.foodsharing-staedte.org vor.

Kontakt:

foodsharing-städte-Pressestelle:  
Veronika Hempe, Clara Brossmann  
presse@foodsharing-staedte.org  
www.foodsharing-staedte.org  

Über foodsharing: Seit rund sieben Jahren rettet die mehrfach ausgezeichnete foodsharing-Bewegung täglich tonnenweise genießbare Lebensmittel vor der Entsorgung und verteilt sie ehrenamtlich und kostenfrei im Bekanntenkreis, in Obdachlosenheimen, Kindergärten und über die Plattform www.foodsharing.de. Über 297.000 Menschen aus Deutschland, Österreich und der Schweiz nutzen regelmäßig die Internetplattform im Sinne „Teile Lebensmittel, anstatt sie wegzuwerfen!“. Darüber hinaus engagieren sich rund 66.000 Menschen ehrenamtlich als Foodsaver*innen, indem sie überproduzierte Lebensmittel von Bäckereien, Supermärkten, Kantinen und Großhändlern abholen und verteilen. Das geschieht kontinuierlich über 2.500 Mal am Tag bei über 6.000 Kooperationspartnern.

