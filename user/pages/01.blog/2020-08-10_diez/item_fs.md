---
title: 'Die fünfte im Bunde: Diez ist foodsharing-Stadt'
date: '14:00 08/10/2020'
taxonomy:
    category:
        - blog
first_image: 2020-08-10_diez/diez_foodsharing-stadt-2020.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir freuen uns sehr darüber, den Bezirk Limburg, Diez und Umgebung
als neue foodsharing-Stadt begrüßen zu dürfen. Zusammen mit Eupen,
Graz, Jena, und Remscheid ist Diez damit die fünfte Stadt, die wir
als offizielle foodsharing-Stadt auszeichnen dürfen.

Seit 2017 haben die
Foodsaver\*Innen aus Limburg, Diez und Umgebung Kooperationen mit 201
Betrieben bewerkstelligt. Mit rund 50 davon konnte sogar eine
laufende Kooperation erreicht werden,  die dazu dienen, beständig
gegen die Lebensmittelverschwendung vorzugehen. Gerettete
Lebensmittel werden an die Flüchtlingsinitiative “Willkommenskreis
Diez”, andere soziale Einrichtungen oder via Essenkorb verteilt.

Durch die sozialen
Medien und auch durch die Öffentlichkeitsarbeit des
Willkommenskreises hat foodsharing in Diez einen sehr hohen
Bekanntheitsgrad erreicht. Mithilfe regelmäßiger Infostände und
der wöchentlichen Fairteilung im Willkommenskreis werden Bürger\*Innen
miteinbezogen und Aufklärungsarbeit geleistet. Geplant sind auch
weitere Zusammenarbeiten mit der Aktionsgemeinschaft „Blühende
Lebensräume“, mit Schulen und Kindergärten sowie mit einem
Unverpackt-Laden, der in Kürze öffnen wird. 

Das besondere
Engagement der Diezer Foodsaver\*Innen wurde sogar von Landrat Puchtler
und die RLP Ministerpräsidentin Malu Dreyer ausgezeichnet.

Wir sind begeistert
über eine so motivierte und engagierte foodsharing-Stadt.

*Von Vroni*

