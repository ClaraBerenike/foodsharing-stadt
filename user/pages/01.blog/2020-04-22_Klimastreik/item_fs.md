---
title: '#NetzstreikfürsKlima - globaler Klimastreik am 24. April findet digital statt'
date: '12:00 04/22/2020'
taxonomy:
    category:
        - blog
first_image: 2020-04-22_Klimastreik/klimastreik1.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---

Rund 630.000 Menschen beteiligten sich nach Angaben von Fridays for
Future am letzten Klimastreik im Novemer 2019, um gemeinsam für den
Klimaschutz einzutreten. Allein in Berlin sprach die Polizei von rund
100.000 Teilnehmern. 


Trotz Corona-Krise wird es am 24. April
2020 erneut einen globalen Klimastreik geben. Statt auf der Straße
soll die Demonstration im Netz stattfinden. Über den Hashtag
\#Netzstreikfürsklima können sich Streikende in Form von
Livestreams, Videobeiträgen oder Bildern in den sozialen Medien am
Protest beteiligen. Auf [](https://fridaysforfuture.de/neustartklima)
können die Streikenden sich auf einer Landkarte eintragen und sich
Profilbilder mit Streik-Logo generieren, um in den Sozialen Medien
auf die Aktion aufmerksam zu machen und weitere Unterstützer zu
mobilisieren.

![](klimastreik2.jpg)

foodsharing setzt sich zusammen mit der
Bewegung foodsharing-Städte gegen die Verschwendung von
Lebensmitteln ein, schont Ressourcen und ist somit ein Beitrag zum
aktiven Klimaschutz. In Zeiten von Corona hat sich bereits gezeigt,
wie wichtig Solidarität und Zusammenhalt ist. Lasst uns diese
Energie auch für den Klimawandel aufbringen. Unterstützt den
Protest digital, platziert eure Plakate und Schilder gut sichtbar im
Fenster oder auf dem Balkon und seid am 24. April ab 12 Uhr im
Live-Stream auf [](https://fridaysforfuture.de/netzstreikfursklima/)
dabei!

*von Vroni*