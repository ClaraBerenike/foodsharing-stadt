---
title: 'Tag der Lebensmittelverschwendung'
date: '12:00 05/02/2020'
taxonomy:
    category:
        - blog
first_image: 2020-05-02_Tag_der_LMVersch/10_Blogpost_Muelleimer.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---

Im Jahr 2016 rief der World Wide Fund For Nature (WWF) zum ersten Mal
dazu auf, den 2. Mail zum “Tag der Lebensmittelverschwendung” zu
ernennen. Aus welchem Grund wurde dazu ausgerechnet der 2. Mai
ausgewählt? Statistisch gesehen, landet die gesamte Menge der
zwischen Januar und Mai produzierten Lebensmitteln in der Tonne. Der
Tag der Lebensmittelverschwendung markiert demnach den Tag im Jahr,
an dem wir in Deutschland beginnen, Nahrungsmittel für unseren
Teller und nicht für die Mülltonne zu produzieren. 




Jahr für Jahr
landen in Deutschland rund 13 Millionen Tonnen Lebensmittel im Müll.
Entlang der gesamten Wertschöpfungskette gibt es zahlreiche Gründe,
warum Lebensmittel entsorgt werden. Zu groß, zu klein oder die
falsche Farbe: all das können bereits Gründe dafür sein, dass ein
Lebensmittel durch das Raster fällt und schon bei der Ernte oder im
Supermarkt aussortiert wird. Auch Transportschäden oder falsche
Lagerung können dazu führen, dass das Lebensmittel entsorgt werden,
obwohl diese noch genießbar wären. Ein Großteil der
Lebensmittelverschwendung ist jedoch auf private Haushalte
zurückzuführen: Verbraucherinnen und Verbraucher werfen pro Kopf
etwa 75 Kilogramm Lebensmittel in den Müll. Dabei wirkt sich der
verschwenderische Umgang mit Lebensmitteln nicht nur auf den eigenen
Geldbeutel, sondern auch auf das Klima und die Umwelt aus:
Emissionen, die bei Anbau oder Aufzucht, der Ernte, während der
Logistik und bei der Lagerung dieser Lebensmittel angefallen sind,
landen zusammen mit den Waren selbst in der Tonne.

![Bananen](10_Blogpost_Bananen.jpg)

Die
Bundesregierung hat im Februar 2019 die Nationale Strategie zur
Reduzierung der Lebensmittelverschwendung beschlossen. Ziel ist es,
die Zahl der weggeworfenen Lebensmittel bei Verbrauchern und im
Einzelhandel bis 2030 zu halbieren. Neben der bereits seit 2012
bestehenden Initiative “Zu gut für die Tonne”, die zur
Information und Aufklärung der Verbraucher dient und  die
kreativsten Ideen mit dem Bundespreis “Zu gut für die Tonne”
prämiert, werden Mittel für Forschungsprogramme bereitgestellt, um
Fragen der Ressourceneffizienz, von
Lebensmittelverarbeitungsprozessen oder zum Wegwerfverhalten der
Konsumenten zu analysieren. In Dialogforen bezüglich
unterschiedlicher Schwerpunktthemen wie beispielsweise
“Außer-Haus-Verpflegung” und “Groß- und Einzelhandel”
sollen konkrete Maßnahmen vereinbart und Zielmarken definiert
werden. Konkrete gesetzliche Regelungen hinsichtlich der
Lebensmittelverschwendung, wie es sie beispielsweise in Frankreich
bereits gibt, wurden bisher nicht beschlossen. 





Lockerungen
in Zusammenhang mit der Lebensmittelrettung sind bisher nicht in
Sicht. Momentan haften Lebensmittelhändler, Gastronomen oder
Bäckereien bei Lebensmittelspenden weiterhin gegenüber
lebensmittelrettenden Organisationen für mögliche gesundheitliche
Folgen. Diese wiederum haften auch, wenn sie ihrerseits Lebensmittel
weiterverteilen. Zusammen
mit der Deutschen Umwelthilfe hat die foodsharing-Bewegung daher eine
Petition ins Leben gerufen, die fordert, das Retten von Lebensmitteln
zu vereinfachen. Unter [https://www.change.org/lebensmittel-retten](https://www.change.org/lebensmittel-retten)
wurden bereits 40.000 Unterschriften von Unterstützern der Petition
gesammelt, darunter auch prominenter Support von der deutschen
Schauspielerin Marion Kracht. Die Petition fordert Rechtssicherheit
bei der Lebensmittelrettung durch den gesetzlichen Abbau von
Haftungsrisiken und durch staatliche Unterstützung von
Verteilinfrastrukturen in Zeiten der Corona-Krise sowie darüber
hinaus. Um die Lebensmittelrettung zu erleichtern, werden gesetzliche
Rahmenbedingungen wie in Italien gefordert. Auf Basis des
„Guten-Samariter Gesetzes“ sind dort
lebensmittelrettende
Organisationen von der Haftung befreit. Händler und Gastronomen
erhalten zudem steuerliche Vorteile, wenn sie Lebensmittel spenden.



Links:
Zur
Petition: [https://www.change.org/lebensmittel-retten](https://www.change.org/lebensmittel-retten)