---
title: 'Blog unter Fremdregie - heute: Lübeck'
date: '12:00 12/03/2020'
taxonomy:
    category:
        - blog
first_image:
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
In Zeiten der Corona-Pandemie ist jede\*r Einzelne in ihrem/seinem Alltag mehr oder weniger von den Einschränken betroffen. Auch im Hinblick auf foodsharing ist die Corona-Krise eine Phase, in der wir uns intensiv mit angemessenen Maßnahmen beschäftigen. Es ist häufig ein Spagat
zwischen dem Retten von Lebensmitteln, der Versorgung von Bedürftigen und der Risikovermeidung für Foodsaver und Fairteiler-Nutzer.
Besonders interessant ist da ein Bericht “aus erster Reihe”. Foodsharing Lübeck berichtet über die eigenen ersten Schritte auf
dem Weg in Richtung foodsharing-Stadt und die eigenen Eindrücke und Erlebnisse aus der Corona-Krise.

### Foodsharing in Lübeck

Am 19.11.2018 wurde der Verein foodsharing Lübeck e.V. beim Amtsgericht Lübeck in das zentrale Vereinsregister eingetragen. Im März 2019 bekamen wir vom Finanzamt die Gemeinnützigkeit für unseren Verein bestätigt. Auf www.foodsharing.de fanden wir alle Informationen, die wir für die Vereinsgründung brauchen. Wir waren sehr dankbar über diese Informationen, die uns die Vereinsgründung und die Anerkennung der
Gemeinnützigkeit enorm erleichterten bzw. erst möglich machten.

Die Vereinsgründung war für uns der Startschuss für ein erweitertes Engagement in der Stadt Lübeck über die bisherige Arbeit unserer Initiative hinaus. Ab diesem Zeitpunkt eröffneten wir ein Konto, schlossen eine Vereinshaftpflichtversicherung ab, bemühten uns um Spendengelder und eröffneten drei Fairteiler. All dies war uns vor der Vereinsgründung nicht möglich gewesen.

Uns wurde der Kontakt zu einer Lübecker Stiftung empfohlen, von der wir unkompliziert Unterstützung für T-Shirts und den Aufbau von
Fairteilern erhielten. Einen Fairteiler finanzierten wir über ein Crowdfunding auf der Seite www.betterplace.org. Wir organisierten Schnippelpartys und diverse Informationsstände (z.B. in der Universität oder der Ehrenamtsmesse). Eine Rettung bei Dräger, einem großen Wirtschaftsunternehmen in Lübeck, ermöglichte uns die Eröffnung unseres 3. Fairteilers auf dem Grundstück des Unternehmens.

Um in der Stadt
bekannt zu werden, Verbündete für unsere Arbeit zu finden und noch
mehr Lebensmittel retten zu können, beschlossen wir außerdem, uns
verstärkt an die Kommunalpolitik zu wenden. Gerne möchten wir von
diesen Aktivitäten berichten, damit andere Städte es als „Best
Practice“ nutzen und es uns gleichtun können.

Wir „retteten“ z. B. die Reste von Buffets im Lübecker Rathaus. Während dieser
Rettungen lernten wir den Lübecker Bürgermeister, die Lübecker
Senator\*innen, die Lübecker Stadtpräsidentin sowie eine Reihe von
Kommunalpolitiker\*innen verschiedener Parteien kennen. Eine
Partei in Lübeck – die GAL (Grün-alternativ-links) - stellte
daraufhin im Lübecker Stadtparlament einen Antrag für einen „Runden
Tisch foodsharing“, der von einer sehr breiten Mehrheit in der
Lübecker Bürgerschaft angenommen wurde. Den Antrag inklusive
zugehöriger Argumentation findet ihr [auf der Seite der GAL](http://gal-luebeck.de/tag/foodsaver/).
Vielleicht kann dieser Antrag als Muster für andere Städte dienen,
um an die Stadtverwaltung heranzutreten.

Am 30.03.2020 sollte
der Runde Tisch zum 1. Mal stattfinden. Neben foodsharing lud der
Sozialsenator auch folgende Gruppen ein: der
Lebensmitteleinzelhandel, Gastronomiebetreibende, die Lübecker Tafel
e.V., Marktstandbetreiber\*innen und die Mitglieder der Fraktionen der
Bürgerschaft. Die Moderation des Runden Tisches sowie die
Verantwortung, zu Folgeterminen einzuladen, wurde Foodsharing Lübeck
übertragen. Leider kam die Corona-Krise dazwischen, sodass der 1.
Termin für diesen Runden Tisch auf unbestimmte Zeit verschoben
werden musste.

Obwohl die
Terminverschiebung ein herber Rückschlag für uns war, so zeigte
sich die Corona-Krise jedoch auch als Chance für foodsharing Lübeck:
Aufgrund von Personalengpässen konnte die Tafel übriggebliebene
Lebensmittel nicht mehr regelmäßig bei Supermärkten abholen. Viele
ehrenamtliche Mitarbeiter der Tafel fielen aufgrund ihrer
Zugehörigkeit zur Risikogruppe aus. Die zentrale und nicht-digitale
Organisation der Tafel-Abholungen erschwerten die Situation.
Unser Online-System,
unsere – zum Glück – vielen fitten Foodsaver\*innen und unsere
dezentrale Organisationsform ermöglichten es uns, weiterhin zu
retten und sogar unsere Rettungen noch auszuweiten. Lebensmittel, die
die Tafel aktuell nicht abholen kann und die somit in der Mülltonne
landen würden, werden nun durch foodsharing Lübeck gerettet. Dabei
ist es uns besonders wichtig zu betonen, dass wir nie vorhatten, eine
Konkurrenz für die Tafel zu sein. Unser Ziel war und ist es
lediglich, die Lücke zu schließen und die Lebensmittel zu „retten“,
die trotz des Engagements der Tafel weggeschmissen werden müssten.
Dass diese Lücke in der Corona-Krise größer wurde, machte die
Arbeit von den Foodsaver*innen in Lübeck umso wichtiger.

Um jedoch auch
unsere Foodsaver\*innen angemessen zu schützen, beschlossen wir, pro
Rettung maxmal zwei Foodsaver zuzulassen und Empfehlungen bezüglich
Hygienemaßnahmen (Mundschutz, Handschuhe, Anstand halten) heraus zu
geben. Auch im Hinblick auf unsere Fairteiler ergriffen wir
Maßnahmen: Nachdem es am Anfang der Corona-Krise zu unschönen
Auseinandersetzungen zwischen Lübeck*innen gab, die um Lebensmittel
stritten, entschlossen wir uns die Fairteiler vorerst zu schließen.
Wir brachten rot-weiße Abstandshalter vor den Fairteilern an und
öffneten diese wenige tage später wieder. Seit der Wiedereröffnung
bekam wir keine Meldungen mehr über Unruhen in der Nähe der
Fairteiler. 

Über unsere Arbeit
erzählten wir mit Berichten und Statusmeldungen in den sozialen
Medien (Facebook: https://www.facebook.com/foodsharingluebeck/
Instagram: @foodsharing.luebeck).
Auch die Lübecker Nachrichten, als auflagenstärkste Lokalzeitung,
fütterten wir regelmäßig mit Informationen. Hierzu können wir
anderen foodsharing-Städten nur empfehlen, aktiv den Kontakt zu
Redakteur\*innen zu suchen und Neuigkeiten über kurze Wege
weiterzugeben. Ofmtals genügt anstelle einer Pressemitteilungen ein
kurzer Anruf oder eine kurze E-Mail mit Informationen an eine/n
Redakteur\*in. 

Hier ein paar
Zeitungsberichte über uns seit der Vereinsgründung:
- Lübecker Nachrichten: [„Foodsaver“ retten Nahrung vor der Tonne](https://www.ln-online.de/Nachrichten/Norddeutschland/Foodsharing-Lebensmittel-retten-auf-dem-Weihnachtsmarkt)
- Lübecker Nachrichten: [Lübeck bekommt einen Foodsharing-Schrank](https://www.ln-online.de/Lokales/Luebeck/Luebeck-bekommt-einen-Foodsharing-Schrank)
- Lübecker Nachrichten: [Auch bei Dräger in Lübeck steht jetzt ein Schrank für Lebensmittel ](https://www.ln-online.de/Lokales/Luebeck/Dritter-Lebensmittel-Schrank-in-Luebeck-steht-bei-Draeger)
- Lübecker Nachrichten: [Trotz Corona: Lübecks Lebensmittelretter sind weiter aktiv ](https://www.ln-online.de/Lokales/Luebeck/Luebecks-Lebensmittelretter-sind-trotz-des-Coronavirus-unterwegs)

Zu Beginn der
Corona-Krise kam es in Lübeck zur Gründung von „Gabenzäunen“,
um Bedürftige mit dem Notwendigsten zu versorgen. An diese wurden
häufig auch Lebensmittel gehängt. Aufgrund von Hygienebedenken im
Hinblick auf verderbliche und kühlpflichtige Lebensmittel, empfohlen
wir der Stadt, verstärkt auf foodsharing-Schränke zu setzen. Nach
einem Telefonat mit dem Sozialsenator folgte die Zusage, in
Zusammenarbeit mit der Handwerker-Innung und lokalen
Tischlerei-Betrieben, noch vor Ostern 2-4 Schränke aufzustellen.
Zeitgleich starteten wir einen Aufruf über Facebook mit dem Tenor:
„Wir suchen geeignete Standorte und Freiwillige für die Pflege der
Schränke“ und wurden mit Antworten und Anfragen überschüttet. Um
der Informationsflut zu begegnen, legten wir eine Liste mit den
möglichen Standorten an. Der Sozialsenator und der Bürgermeister
gaben außerdem ihre Einschätzungen ab, in welchen Stadtteilen die
meisten Lebensmittel benötigt würden. Anhand dieser Liste und der
Empfehlungen der Stadtverwaltung wählten wir die Standorte für die
2-4 zukünftigen Fairteiler aus.

Im Moment warten wir
gespannt und voller Freude auf die neuen Fairteiler und führen
fleißig Rettungen bei Supermärkten durch. Wir freuen uns, in der
Corona-Krise Menschen helfen zu können und aktiv sein zu dürfen.
Auch wenn wir uns damit unserem eigentlichen Ziel - irgendwann nicht
mehr gebraucht zu werden - momentan entfernen. Aber: Dafür gibt ja
zum Glück eine Zeit nach Corona.

Unser Fazit und
unsere Empfehlungen für alle foodsharing-Städte (und solche, die es
werden wollen):

- Gründet einen	Ortsverein, ihr habt damit viel mehr Möglichkeiten.
- Kooperiert neben der Tafel auch mit der Stadtverwaltung und der Kommunalpolitik, sucht aktiv den Kontakt. 
- Nutzt die Macht der (sozialen) Medien.

Wir wünschen allen
anderen Städten FROHES RETTEN während und nach der Corona-Krise!

Bei Fragen schreibt uns eine Mail unter: luebeck@foodsharing.network
 
Für das foodsharing-Team Lübeck <br>
Sophie Bachmann
