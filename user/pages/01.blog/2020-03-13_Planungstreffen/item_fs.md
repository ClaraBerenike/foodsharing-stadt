---
title: 'Gemeinsam geht mehr'
date: '12:00 03/13/2020'
taxonomy:
    category:
        - blog
first_image: 2020-03-13_Planungstreffen/Agenda.jpeg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: '
Unser Planungstreffen vom 06.-08. März 2020
'
---
Was waren wir fleißig, um die Bewegung inklusive Website pünktlich zum 12. Dezember 2019 online zu bekommen! Ich erinnere mich, als wäre es gestern gewesen. Genauso gerne erinnere ich mich an das gute Gefühl und die ausgelassene Stimmung im Team, als es endlich so weit war. Seitdem sind fast 3 Monate vergangen. Ein neues Jahre hat begonnen und unsere Bewegung foodsharing-Städte ist - fast von ganz allein - noch bekannter geworden.

![Planen und Wuppen](PlanenWuppen.jpeg)

Aber: Wer sind eigentlich diese Menschen hinter der Bewegung?
DAS habt nicht nur ihr euch gefragt! Zusammengefunden hat sich das ich das Orgateam der Bewegung foodsharing-Städte nämlich online. Wir kommunizieren meist elektronisch via Textnachrichten sowie mithilfe von Video- und Telefonkonferenzen. Unser Bild voneinander beschränkt sich daher maximal auf das verpixelte Bild der Handy- oder Webcam während unserer regelmäßigen Video-Konferenzen. 

![Agenda](Agenda.jpeg)

Um uns nun endlich auch persönlich kennen zu lernen, unsere bisherige Arbeit zu reflektieren, erreichte Erfolge zu feiern und neue Ziele zu setzen haben wir uns am vergangenen Wochenende in Darmstadt getroffen, wo unsere liebe Clara uns in ihrer WG willkommen hieß. Unser Team umfasst aktuell 12 Mitglieder\*innen aus allen Teilen der Republik. Teils krankheitsbedingt, teils aufgrund privater Umstände konnten an unserem ersten Treffen leider nicht alle Teammitglieder*innen teilnehmen. Nichts desto trotz haben wir das Wochenende genutzt und uns gemeinsam Gedanken über die Bewegung foodsharing-Städte, das Vergangene und die Zukunft gemacht. 

![Planen und Wuppen die zweite](PlanenWuppen2.jpeg)

Dank einer strukturierten Agenda und unterschiedlichen Methodiken aus der Bildungsarbeit haben wir auf effiziente Art und Weise das bisher Erreichte reflektiert und uns darauf aufbauend Ziele für die Zukunft ausgemalt. Auch unsere gemeinsame Arbeit, unsere Teamzusammensetzung und konkrete Maßnahmen, um die gesetzten Ziele zu erreichen, waren Themen über die wir uns am vergangenenen Wochenende ausgetauscht haben. So haben wir uns beispielsweise zum Ziel gesetzt, bis zum 1. Geburtstag unserer Website 25 Foodsharing-Städte und 50 “Städte auf dem Weg” zu unserer Bewegung zählen zu können. Das wäre ne Wucht, oder?

![Kochen](Kochen.jpeg)

Stets gestärkt mit geretteten Lebensmitteln und voll integriert von Claras WG-Mitbewohnern, haben wir nicht nur ein produktives, sondern noch dazu ein sehr schönes und ereignisreiches Wochenende in Darmstadt verbracht. Nach so viel Input, Austausch und neuen Ideen freuen wir uns schon jetzt auf unser nächstes Orgateam-Treffen, welches im Herbst 2020 stattfinden soll. Ganz besonders freuen wir uns dann darauf, auch noch all unsere anderen Teammitglieder persönlich kennen zu lernen.

*von Vroni*