---
title: 'Blog unter Fremdregie - heute: Remscheid'
date: '12:00 04/16/2020'
taxonomy:
    category:
        - blog
first_image: 2020-04-16_Remscheid/remscheid_bild3.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---

Mithilfe unseres Blogs möchten wir einen Einblick in die Bewegung foodsharing-Städte geben und zeigen, welche Potentiale in dieser Vision stecken - und wer könnte das besser, als unsere (angehenden) foodsharing-Städte selbst? Aus diesem Grund werden wir künftig unter dem Motto “Blog unter Fremdregie” regelmäßig Geschichten und Berichte aus der Feder unserer teilnehmenden Städte posten.

### Remscheid - Unser Weg zur foodsharing-Stadt

Als ich im Dezember 2019 den ersten Hinweis auf das Projekt „foodsharing-Stadt“ auf der Website entdeckte, war ich sofort begeistert. Das ist es, wofür ich schon immer plädiert habe: Ein gutes Netzwerk ist das A und O und wenn wir es schaffen, viele Initiativen und die öffentliche Hand an einen Tisch zu bekommen, kann das für alle Beteiligten und für unser gemeinsames Ziel, die Lebensmittelverschwendung zu reduzieren, nur von Vorteil sein!
Mit meiner Botschafterkollegin war ich da einer Meinung und so machte ich mich ans Werk und ging den Ideenkatalog Schritt für Schritt durch.
Ich muss gestehen, dass die Bewerbung mich schon viel Zeit gekostet hat. Ungefähr 3 Tage hab ich daran gesessen, Texte formuliert, entsprechende Fotos rausgesucht und überlegt, was wir denn so alles machen in Remscheid.
Das ist dann auch direkt eine Sache, die einfach großartig ist: Es macht zwar Arbeit aber wenn man dann alles fertig hat, ist es toll und beeindruckend mal konzentriert zu sehen, was der Bezirk schon alles erreicht hat. Wieviele Zahnrädchen da in einander greifen, wieviele verschiedene Charaktere zusammen wirken und wieviele Menschen ihre Zeit investieren, um foodsharing wachsen zu lassen.

Im Oktober 2016 fand foodsharing seinen Weg nach Remscheid. 5 Menschen engagierten sich -heute sind wir 120 aktive Foodsaver.
Inzwischen haben wir ca. 50 Kooperationen, diverse private Fairteiler über die Stadt verteilt und einen engen Kontakt zu den unterschiedlichsten sozialen Initiativen.
Bereits zu Beginn 2016 hatte Elisabeth Erbe mit der Tafel Remscheid Kontakt aufgenommen. Damals war die Zusammenarbeit noch etwas zögerlich und skeptisch, heute treffen wir uns regelmäßig zu einem runden Tisch und übernehmen jetzt in der Corona-Zeit sogar eine Ausgabestelle, die durch die ehrenamtlichen Helfer der Tafel derzeit nicht besetzt werden kann. Auch mit dem Bildungsbüro der Stadt Remscheid gibt es bereits seit ca. 3 Jahren eine gute Kooperation mit dem Schwerpunkt “Bildung für nachhaltige Entwicklung”. Wir halten Vorträge an Schulen, kochen Marmelade mit den Schülern oder bieten Smoothies auf einer Jugendparty unter dem Motto „zero waste“ an.

Im November 2019 wurde unser Engagement dann mit der Ehrenamtsnadel für ehrenamtliche Tätigkeit durch den Oberbürgermeister Burkhard Mast-Weisz honoriert. 

![Ehrenamtsnadel für ehrenamtliche Tätigkeit](remscheid_bild1.jpg)
![Ehrenamtsnadel für ehrenamtliche Tätigkeit](remscheid_bild2.jpg)
*Fotos: Mirjam Starke*

Bei der feierlichen Verleihung im Rathaus nutzte ich die Gelegenheit und bat um eine Verlinkung von www.foodsharing.de auf der Homepage der Stadt. Außerdem fragte ich den Oberbürgermeister, ob es denn im Rathaus selber nichts für uns Foodsaver zu tun gäbe :-) .
Ich freue mich, dass wir einen sehr engagierten und offenen Bürgermeister haben, der voll und ganz hinter foodsharing steht: Er sorgte für die Verlinkung auf Remscheids Homepage und lud uns ein, von nun an bei allen Veranstaltungen und Empfängen im Rathaus die Reste des Buffets zu retten. Ich glaube, das war der Grundstein für eine wirklich hervorragende Zusammenarbeit mit der Stadt.

Herr Mast-Weisz war daraufhin auch sofort bereit, die gemeinsame Motivationserklärung der foodsharing-Städte zu unterzeichnen. Dies geschah vor der örtlichen Presse am 12. März 2020 gemeinsam mit dem Vorstand der Remscheider Tafel, Vertretern des Bildungsbüros und dem Sozialderzernent. Zusätzlich hatte uns einige Tage zuvor auch der WDR begleitet und über dieses Projekt berichtet.

Wir sind stolz: **Remscheid ist die 1. foodsharing-Stadt Deutschlands!**

Ich kann nur allen foodsharing-Bezirken ans Herz legen, sich aufzumachen und sich zu bewerben.
Es wäre toll, wenn die virtuellen Bäume der foodsharing-Städte zu einem großen Wald heranwachsen würden. Für uns hat es sich sehr rentiert. Wir sind nun noch besser vernetzt, haben Kontakt zu den verschiedenen städtischen Ämtern, die Korrespondenz erfolgt schnell und unbürokratisch, wir sind gern gesehene Koopertionspartner in den verschiedensten Bereichen und bei den unterschiedlichsten Initiativen. Durch die Präsenz in der Presse ist unser Bekanntheitsgrad weiter gestiegen und auch sonst eher skeptische Betriebe, stimmen nun einer Zusammenarbeit zu.

Geht es an! Und werdet auch ihr eine foodsharing-Stadt!

Wenn ihr Fragen habt oder Hilfe benötigt, wendet euch gerne an das Orga-Team der foodsharing-Städte oder auch an mich persönlich.

Mirjam Starke

![Unterzeichnung](remscheid_bild3.jpg)
*Foto: Mirjam Starke*
