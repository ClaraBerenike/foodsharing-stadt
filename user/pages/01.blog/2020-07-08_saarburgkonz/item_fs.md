---
title: 'Saarburg-Kell und Konz wird Teil der foodsharing Städe-Bewegung'
date: '10:00 07/08/2020'
taxonomy:
    category:
        - blog
first_image: 2020-07-08_saarburgkonz/saarburg.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir begrüßen die foodsharing-Ortsgruppe Saarburg-Kell und Konz als
neustes Mitglied unserer Bewegung. Erst seit Januar 2020 gibt es den
foodsharing-Bezirk, in dem sich Foodaver\*innen aus der Stadt Saarburg
und Konz sowie der Verbandsgemeinde Saarburg-Kell engagieren.

In dieser kurzen
Zeit wurden bereits 13 Kooperationen erschlossenen. Etwa 51
Foodsaver\*innen holen in Saarburg-Kell und Konz übrig gebliebene
Lebensmittel aus Bäckereien, Supermärkten, von Gemüsehändlern und
Käse-Manufakturen ab. 

Darüber hinaus
konnten bereits zwei öffentliche Fairteiler eingerichtet werden, mit
deren Hilfe Lebensmittel kontaktlos geteilt werden können.

Ganz besonders
gefällt uns die Aktion, die einmal im Monat in Konz stattfindet:
Unter dem Motto Küche für alle (KüfA) schnibbeln und kochen viele
Leute gemeinsam. Dabei ist Jede\*r willkommen - einzige Bindung ist
der respektvolle Umgang miteinander.

Wir freuen uns, dass Saarburg-Kell und Konz nun Teil unserer Bewegung
sind und freuen uns, sie auf ihrem Weg zur foodsharing-Stadt zu begleiten.

*Von Vroni*
