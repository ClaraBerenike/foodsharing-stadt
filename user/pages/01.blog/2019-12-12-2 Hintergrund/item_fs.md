---
title: 'Hintergrundinfos'
date: '18:00 12/12/2019'
taxonomy:
    category:
        - blog
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: 'rund um die Bewegung foodsharing-Städte und die Website'
---

##### Wie wurden die ersten Städte ausgewählt?
Um die Website schon vor dem Go-Live mit Leben zu füllen, haben wir uns entschieden, bereits im Vorfeld einige Städte für unsere Bewegung zu gewinnen. Dies war kein willkürlicher Prozess: Vielleicht können sich  einige von euch erinnern, dass es dieses Jahr einen Aufruf bei foodsharing.de gab, dass Städte Poster für das foodsharing Festival gestalten können, auf dem sie ihre Aktivitäten darstellen. Dazu waren alle aus der Community eingeladen. Alle Städte, die diesem Aufruf gefolgt sind, haben wir nach dem Festival angeschrieben und gefragt, ob sie Lust haben, Teil der Bewegung foodsharing-Städte zu werden. Das Ergebnis könnt ihr jetzt hier bestaunen! Wir sind stolz, dass wir so schnell, so viele Menschen von unserer Idee überzeugen konnten.

##### Ist die Website eigentlich schon fertig? 
Fertig ist so ein Projekt wohl nie wirklich, aber sie ist so weit fertig, dass wir uns wohl damit fühlen, sie online zu stellen. Trotzdem wollen wir noch weiter daran basteln, - sowohl optisch als auch inhaltlich – um das Engagement der foodsharing-Städte, unseren Ideenkatalog und den Gerätschuppen optimal darzustellen. Hoffentlich werden sich nach und nach immer mehr Städte auf unserer Karte eintragen lassen, ihr aktuelles Engagement auf unserer Webseite präsentieren und sich mithilfe unseres Ideenkatalogs zu noch mehr Engagement inspirieren lassen. 