---
title: Mach mit!
visible: false
published: false


form:
    name: contact

    fields:
        - name: name
          label: Name
          placeholder: Enter your name
          autocomplete: on
          type: text
          validate:
            required: true

        - name: email
          label: Email
          placeholder: Enter your email address
          type: email
          validate:
            required: true

        - name: message
          label: Message
          placeholder: Enter your message
          type: textarea
          validate:
            required: true

        - name: g-recaptcha-response
          label: Captcha
          type: captcha
          recaptcha_site_key: ENTER_YOUR_CAPTCHA_SITE_KEY
          recaptcha_not_validated: 'Captcha not valid!'
          validate:
            required: true

    buttons:
        - type: submit
          value: Absenden
        - type: reset
          value: Reset

    process:
        - captcha:
            recaptcha_secret: ENTER_YOUR_CAPTCHA_SECRET_KEY
        - email:
            subject: "[Site Contact Form] {{ form.value.name|e }}"
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: contact-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: Thank you for getting in touch!
        - display: thankyou
---

#Werde foodsharing-Stadt!

Zusammen wollen wir Lebensmitteln ihren Wert zurückgeben und schonend mit unseren Ressourcen umgehen. Als foodsharing-Stadt setzt ihr euch besonderns im Bereich Lebensmittelverschwendung- und wertschätzung ein, damit wir dieses Problem zusammen lösen können!

Jede Bewegung startet mit einem Keim. Daraus wächst langsam die Wurzel. Über die Zeit wird aus der zarten Pflanze ein kräftiger Stamm, der unsere vereinten Kräfte symbolisiert. Die prächtige Krone zeugt von den erzielten Erfolgen. Eines Tages werden wir die Früchte unserer Arbeit ernten können.

Schreibe uns an: