---
title: "foodsharing in Zeiten von Corona"
description: "foodsharing in Zeiten von Corona"
published: true
---

### Die aktuelle Situation erfordert einen kühlen Kopf: Wir möchten die Lage ernst nehmen, ohne Panik zu verbreiten. Wir sollten überlegen, wie wir einen Teil dazu beitragen können, dass wir als Gesellschaft gut durch diese Zeit kommen. 

Dazu kann gehören, dass wir uns selbst und andere schützen, indem wir z. B. Abstand halten oder dass wir überlegen, wie wir andere Menschen aktiv unterstützen können, indem wir ihnen z. B. gerettete Lebensmittel zur Verfügung stellen.

Wie geht ihr in eurer Stadt mit der Corona Situation um? Welche Maßnahmen habt ihr innerhalb des foodsharing Alltags getroffen
(Fairteiler-Schließung, Reduzierung der Anzahl an Abholenden etc.)? Oder wie solidarisiert ihr euch als foodsharing vor Ort aktuell?

![](/user/images/icons/corona.jpg)