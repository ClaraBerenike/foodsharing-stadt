---
title: "Förderung von Alternativen"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Die öffentliche Hand setzt sich dafür ein, dass lokaler Anbau und alternative Geschäftsmodelle (z. B. Unverpackt Läden, Bioläden, Solidarische Landwirtschaft o. Ä.) gefördert werden.

Förderung kann vieles bedeuten: Steuervorteile, eine Quote für alternative Geschäftsmodelle in Einkaufszentren oder bei Ausschreibungen der Stadt, Berücksichtigung bei der Stadtplanung, vergünstigte Miete in Gebäuden der Stadt oder die Bereitstellung von Orten zur Verteilung der Ernte aus der solidarischen Landwirtschaft- vieles ist denkbar. Auch die Sichtbarkeit der Stadt als Unterstützerin des Projekts kann schon viel bewirken. 

![](/user/images/icons/18_idea.jpg)