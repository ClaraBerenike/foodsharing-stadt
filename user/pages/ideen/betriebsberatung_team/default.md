---
title: "Betriebsberatung - Team"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es findet sich innerhalb des Organisationsteams vor Ort eine Kooperations-Gruppe, die sich für die Intensivierung der Zusammenarbeit mit (Super-) Märkten und Gastronomiebetrieben (z. B. Restaurants, städtische Mensen etc.) einsetzt, Infomaterial bereitstellt und Beratungsangebote bezüglich der Steigerung von Lebensmittelwertschätzung anbietet. 

Es ist toll, dass bereits im ganzen Land sehr viele Betriebe ihre Lebensmittel an foodsharing oder andere gemeinnützige Organisationen spenden. Aber das reicht noch nicht, um die Lebensmittelverschwendung ganz zu beenden. Deshalb könnt ihr proaktiv auf Betriebe zugehen und sie dabei unterstützen ihr Wegwerfverhalten zu verändern. 
Hier findet ihr [Tipps für Betriebe](https://cloud.foodsharing.network/s/YDeGNKqBm72B78B)  - Euch fallen bestimmt noch viele weitere Vorschläge ein. 

![](/user/images/icons/09_idea.jpg)

