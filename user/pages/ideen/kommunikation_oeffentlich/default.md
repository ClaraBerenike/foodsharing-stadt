---
title: "Öffentlichkeitsarbeit der Stadt"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Lebensmittelwertschätzung ist in die Kommunikation der Öffentlichen Hand mit eingebunden und wird dadurch im städtischen Raum sichtbar.

Mögliche Ideen sind z. B. Plakate zur Sensibilisierung zu diesem Thema aufzuhängen oder Information über regionale Projekte zum Thema Lebensmittelwertschätzung, sowie über das Ausmaß der Lebensmittelverschwendung auf der Website der Stadt zu präsentieren. Hierfür könnt ihr gerne das Kampagnenmaterial oder andere von foodsharing erstellte Materialien nutzen.

![](/user/images/icons/12_idea.jpg)