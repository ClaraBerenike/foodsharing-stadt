---
title: "Lokale foodsharing Gruppe"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es gibt eine lokale foodsharing Gruppe vor Ort.


Vielleicht sind einige von euch selbst Mitglied in der lokalen foodsharing Gruppe, dann könnt ihr diesen Punkt direkt abhaken. Wenn nicht, nehmt Kontakt zu der foodsharing Gruppe in eurer Stadt auf. Sie ist Expertin auf diesem Gebiet und bestimmt eine gute Partnerin. Gibt es noch keine lokale foodsharing Gruppe, findet ihr [hier Tipps](https://wiki.foodsharing.de/Bezirk_gr%C3%BCnden_oder_reaktivieren), wie ihr eine gründen könnt: 

![](/user/images/icons/01_idea.jpg)