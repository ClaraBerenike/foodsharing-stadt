---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver:  197
- Engagierte über Abholungen hinaus: 14
- Kontakt: rostock@foodsharing.network
