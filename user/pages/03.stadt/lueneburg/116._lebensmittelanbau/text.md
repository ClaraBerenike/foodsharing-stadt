---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: right
---

## Lebensmittelanbau

In Lüneburg gibt es die solidarische Landwirtschaft WirGarten, die als Genossenschaft ca. 500 Mitglieder hat. Auf 8,23 ha Anbaufläche werden dort ökologisches, saisonales und regionales Gemüse mit dem Konzept regenerativer Landwirtschaft produziert. Mehr Infos zum WirGarten findest du unter diesem [Link](https://lueneburg.wirgarten.com/) und im [Lünepedia Eintrag](https://www.luenepedia.de/wiki/WirGarten). Am Rande des Stadtgebiets befinden sich zudem viele landwirtschaftliche Betriebe, die unter anderem Kartoffeln anbauen. Im Spätsommer 2020 hat eine erste Nachernte durch foodsharing stattgefunden und die Kooperation soll fortgeführt und eventuell auf weitere Betriebe und Feldfrüchte ausgeweitet werden. 


