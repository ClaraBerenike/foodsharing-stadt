---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

2020 waren wir auf dem Markt der Möglichkeiten des Klimacamps mit einem Stand dabei, haben mit Interessierten übers Lebensmittelretten gesprochen und leckeres Obst und Gemüse verteilt. Außerdem bemühen wir uns, durch Publikationen und Internet-Präsenz auch für Menschen zugänglich zu sein, die nicht auf der foodsharing-Plattform registriert sind. Ihr könnt uns über eine facebook Gruppe erreichen [](https://www.facebook.com/groups/321037511397295). Außerdem findet ihr mehr Infos in unserem [Lünepedia Eintrag](https://www.luenepedia.de/wiki/Foodsharing) und in der [Artikelsammlung zu foodsharing](https://www.luenepedia.de/wiki/Foodsharing#Berichterstattung). 
