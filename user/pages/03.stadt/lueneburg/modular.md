---
title: Lüneburg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 53.25144
        lng: 10.41316
    status: pending

content:
    items: @self.modular
---
