---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona
Die COVID-19-Pandemie hat auch die Tätigkeiten von foodsharing in Lüneburg beeinflusst, allerdings bei weitem nicht so stark getroffen, wie viele Betriebe und Unternehmungen. So finden Abholungen bei kooperierenden Betrieben weiterhin statt – allerdings mit geringerer Personenzahl. Die Fairteiler sind weiterhin geöffnet – allerdings dürfen diese nur noch einzeln und mit Maske genutzt werden. Unsere Plena haben sich in den virtuellen Raum verschoben. Es hat sich jedoch auch die Stärke der foodsharing-Organisation und des foodsharing-Netzwerks in der Pandemie gezeigt. So gibt es teilweise anhaltende private Unterstützung von besonders stark von der Krise getroffenen Personen mit Lebensmitteln, teilweise konnten wir auch für die Tafel einspringen. Die ohnehin angelegte online-Kommunikation und -Organisation erwies sich in der Pandemie auf jeden Fall als Stärke.   
