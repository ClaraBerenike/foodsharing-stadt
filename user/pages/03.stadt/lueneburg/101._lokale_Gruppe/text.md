---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

foodsharing gibt es in Lüneburg seit Januar 2014. Es gibt aktuell über 250 Personen, die im Bezirk Lüneburg registriert sind. Bis April 2021 haben sich über 100 foodsaver*innen an Abholungen in über 20 Betrieben beteiligt (Tendenz steigend). Hierbei stehen wir in Absprache mit der Tafel, um den Vorrang dieser zu gewährleisten. Insgesamt sind über 100 Betriebe in Lüneburg erfasst. Es wurden ca. 165.000 Kilogramm Lebensmittel gerettet. In den monatlichen Plena sind wir meist ca. 10 Personen. 
