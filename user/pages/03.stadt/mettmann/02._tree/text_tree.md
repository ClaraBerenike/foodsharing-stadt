---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Mettmann
- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 3
- Art der Kooperationen: Marktstand, Tafel, einmalige Rettungsaktionen
- Besonderes: Zum Landkreis Mettmann gehören zehn Städte, darunter Mettmann. Es gibt unterschiedliche Auffassungen, ob mit dem fs-Bezirk Mettmann die Stadt oder der Kreis gemeint ist. Viele Foodsaver im Bezirk Mettmann wohnen nicht dort. In dieser dicht besiedelten Gegend sind die Grenzen fließend.


(Stand: Dezember 2019)
