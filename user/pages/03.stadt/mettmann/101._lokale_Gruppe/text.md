---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 72
- AbholerInnen mind. einmal im Monat: ca. 10
- Engagierte über Abholungen hinaus: 5