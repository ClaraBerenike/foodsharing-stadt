---
title: Info Stände
menu: info_staende
image_align: left
---

## Info Stände

Am Sonntag den 8.7.18 gab es einen foodsharing Infostand auf der Memo 
Nachhaltigkeitsmesse in der Kulturvilla. 
[Mehr Informationen](http://www.memo.jetzt/)