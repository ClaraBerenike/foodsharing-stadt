---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
---

## Gemeinschaftliche Events

Kooperation mit der Abfallwirtschaft der Stadt Stuttgart (AWS) z.B. kostenlose Ausleihe von (neuen) Mülltonnen für die Öffentlichkeitsarbeit.
