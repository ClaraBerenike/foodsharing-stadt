---
title: Stuttgart
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 48.77843
        lng: 9.18002
    status: pending

content:
    items: @self.modular
---
