---
title: Events
menu: events
image_align: left
---

## Events

Als einmalige Sensibilisierungs-Aktion haben wir Chips aus Mülltonnen verteilt.
Seht euch dazu hier unser [Video](https://www.youtube.com/watch?v=20s79G8JSFQ) an.
