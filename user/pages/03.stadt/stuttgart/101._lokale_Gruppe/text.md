---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 1496
- AbholerInnen mind. einmal im Monat: ca. 300
- Engagierte über Abholungen hinaus: ca. 50
- Kontakt: <stuttgart@foodsharing.network>