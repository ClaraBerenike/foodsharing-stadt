---
title: foodsharing in Zeiten von Corona
menu: corona
published: right
image_align: right
---

## foodsharing in Zeiten von Corona
Auf Grund der Pandemie hatte sich das foodsharing Café Raupe Immersatt entschlossen, vorübergehend zu schließen. Trotzdem wurden in Stuttgart weiter Lebensmittel fairteilt. An vier Gabenzäunen in der Stadt (am Marienplatz, der Neckarstraße/Metzstraße, unter der Paulinenbrücke und am Züblin Parkhaus) haben Bewohner\*innen Lebensmittel und Hygieneartikel für andere zum Mitnehmen aufgehängt. Seit Mitte Mai hat der Fairteiler in der Raupe Immersatt wieder geöffnet und auch das Café empfängt wieder Besucher\*innen- allerdings mit einigen neuen Regeln. 
