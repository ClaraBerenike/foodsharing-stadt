---
title: Förderung von Alternativen
menu: alternativen
image_align: left
---

##Förderung von Alternativen
Die Stadt Stuttgart fördert Urban Gardening.
Auf dieser [Webseite](https://www.stuttgart.de/urbanegaerten) findet ihr mehr Infos über urbane Gärten in unserer Stadt.