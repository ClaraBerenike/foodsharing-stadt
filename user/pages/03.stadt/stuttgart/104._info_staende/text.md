---
title: Info Stände
menu: info_staende
image_align: left
---

## Info Stände
Auf diesen Veranstaltungen waren wir als lokale foodsharing-Gruppe vertreten:
*  Zirkus Mutter Erde Festival
*  Übermorgen Markt (s. Foto)
*  Heldenmarkt
*  Fair Handeln
*  Morgenmacher Festival

