---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler
Der Fairteiler ist eine (neue) Hütte und steht auf dem Grundstück der evangelischen Kirche,
die uns dieses zur Verfügung gestellt hat. Die Hütte selbst wurde vom Stadtbezirk gesponsert
Die Webseite des Fair-Teilers findet ihr [hier](https://foodsharing.de/?page=fairteiler&bid=48&sub=ft&id=1145).