---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Wurzen
- foodsharing Bezirk seit September 2017
- Anzahl Kooperationen: 3
- Art der Kooperationen: einen Markstand (wöchentlich), einen Hofladen (2x wöchentlich mit Anruf), ein Reformhaus (unregelmäßig) 
- Besonderes: Wir sind ein ungewöhnlicher Bezirk, weil alle unsere Aktivitäten vom Kanthaus ausgehen. Bisher ist es uns nicht gelungen mehr Menschen aus Wurzen wirklich mit einzubeziehen. Wir haben zwar eine facebook-Gruppe, in der manchmal etwas angeboten wird, aber Abholungen und Planungsarbeit machen zum Großteil wir Kanthausianer.

(Stand: Dezember 2019)


 
 




