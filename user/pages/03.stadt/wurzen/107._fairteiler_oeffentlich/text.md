---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler

In der Wenceslaigasse 15 steht ein stets zugänglicher Fairteiler. Das Projekt wurde von der foodsharing-Ortsgruppe Wurzen initiiert und in Zusammenarbeit mit der Stadtverwaltung Wurzen umgesetzt. Der eigens designte und gezimmerte Fairteilerschrank wurde mit Mitteln des Stadtfonds aufgestellt und wird nun von der foodsharing-Ortsgruppe gepflegt.
