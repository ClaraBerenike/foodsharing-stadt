---
title: Wurzen
menu: Home

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.36707
        lng: 12.74074
    status: approved 

onpage_menu: false

body_classes: 

content:
    items: @self.modular
---
