---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: right
---

## Gemeinschaftliche Events


Im Herbst 2020 haben wir eine öffentliche Aktion auf dem Marktplatz abgehalten, bei der sich Menschen aus dem Ort kleine Kartoffeln aus einer Kartoffel-Steine-Mischung herausklauben konnten, die wir bei einem kooperierenden Hof gerettet haben. Das Ganze war mit der Stadt abgesprochen, wir haben den Ort sowie Strom zur Verfügung gestellt bekommen und durften vorher auch Plakate aufhängen, um die Aktion zu bewerben.
