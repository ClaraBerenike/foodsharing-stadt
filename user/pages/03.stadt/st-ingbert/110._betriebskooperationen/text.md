---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: true
image_align: right
---

## Betriebskooperationen

Die Videothek „Tape-o-mania“ als Platz- und Stromsponsor für unseren Fairteiler.
Der Laden „Unverpackt“ im Bezug auf unverpackte Lebensmittel.
Das „Hofländle“, ein neuartiger Bioladen mit regionalen Produkten.
Ein veganes Restaurant mit mehreren foodtrucks „World Food Trip“ stellte die Speisekarte um.
Das Kreativcafé „Grüne Neune“ stellt uns Räumlichkeiten für Treffen und Aktionen kostenlos zur Verfügung.


