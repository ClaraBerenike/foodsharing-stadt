---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events

Die Biosphären-VHS der Stadt St. Ingbert stellt uns kostenlos auf Anfrage Räumlichkeiten zur Verfügung, in denen Infoabende stattfinden.
Die städtische Pressestelle informierte sowohl auf der facebook-Seite als auch in der Lokalzeitung (Rundschau) über den Faiteiler.