---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Wir wurden vom Warndt Gymansium Völklingen angefragt, ob wir einen Tag anlässlich der UNESCO-Projekttage vom 23. – 27.09.2019 zum Thema „Alles im Überfluss – Alles im Übermaß“ mitgestalten könnten und bei der Präsentation der erarbeiteten Ergebnisse im Rahmen des Schulfests am 28.09.2019 Lebensmittel zum Fairteilen zur Verfügung stellen könnten.
Unsere Botschafterin Nora-Moirin Löffler erklärte sich dazu bereit und stellte auch abgesehen von Foodsharing noch andere Initiativen vor, die sich der Nachhaltigkeit verschrieben haben und mit foodsharing zusammenarbeiten. 
Die Schüler\*Innengruppe bestand aus über 20 Personen der Klassenstufe 9-12. 

Ein kurzer Bericht:
Im Rahmen der Projektwoche wurde foodsharing und andere nachhaltige Initiativen vorgestellt. Mehrere Fairteiler, Tauschregale und ein Urban Gardening Projekt besucht. Im Rahmen des Schulfests wurde eine Verteilung und ein Kleidertausch selbstständig von Schüler\*Innen organisiert. Als der Tag dann endete und die restlichen Lebensmittel wieder mitgenommen werden sollten, um sie zuhause weiter zu verteilen, mobilisierten einige der Schüler\*innen ihre Verwandtschaft, warfen die Pläne für das Abendessen ihrer Eltern um und retten die meisten Lebensmittel selbst. 
Im Nachhinein gab es viele Rückmeldungen, dass nun verstärkt Apps genutzt werden, bewusster eingekauft wird und einige zwischendurch Fairteiler nutzen. Auch gerade Eltern bestätigen, dass das Thema Lebensmittel(-verschwendung) viel stärker in den Fokus gerückt ist.
Bedauert haben alle, dass der Brief, den eine Schüler*innengruppe an die Stadt Völklingen und die Händlergemeinschaft geschickt haben, nicht dazu führte, einen Standort für einen Fairteiler in Völklingen zu finden.
Die Wiederholung einer Zusammenarbeit mit Foodsharing und weitere Aktionen im Rahmen von Schulfesten sind bereits eingeplant.
