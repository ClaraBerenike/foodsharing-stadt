---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: right
---

## Weitere lokale Initiativen

Es gibt um das Rathaus herum den sogenannten Rathausgarten, der mit heimischen Gemüse-, Obst- und Kräutersorten bestückt und für jeden zugänglich ist. 
Die Aussaat und Pflege erfolgt durch Mitarbeiter der Stadt in Zusammenarbeit mit Obst- und Gartenbauverein und Schulen/Kitas.
