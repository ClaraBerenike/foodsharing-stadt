---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Unsere Gruppe besteht zur Zeit aus 60 Stammbezirkler\*Innen – 219 foodsaver\*Innen sind engagiert in St. Ingbert. „Geleitet“ wird dies von 4 Botschafter\*Innen und mehreren Betriebsverantwortlichen.
Mit 19 Betrieben bestehen aktive Kooperationen. Es gibt auch ein Orgateam, welches zur Zeit coronabedingt leider nicht viel tun kann. Seit Bestehen (2018) wurden bei 2134 Abholungen 10829 kg Lebensmittel gerettet .

(Stand: 11.02.21)
