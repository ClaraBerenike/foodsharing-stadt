---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von St. Ingbert

- foodsharing Bezirk seit 2018
- Anzahl Kooperationen: 19
- Besonderes: St. Ingbert ist bereits Fairtrade Stadt und befindet sich in der Biosphäre Bliesgau – ist also auch Biosphären Stadt.


(Stand: 11.02.2021)
