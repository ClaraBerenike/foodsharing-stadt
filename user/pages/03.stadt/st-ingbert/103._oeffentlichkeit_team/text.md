---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Nora-Moirin Löffler, Thomas Debrand, Bianca Thobae sind als Botschafter\*Innen hauptsächlich für die Koordination von Aktionen usw. verantwortlich. Diese werden durch das Orgateam verstärkt. Nora-Moirin Löffler betreut darüber hinaus unsere facebook-Seite und bietet Projekte an Schulen an.
