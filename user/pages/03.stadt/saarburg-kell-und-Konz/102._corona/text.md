---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona

Unsere Betriebe in und um Saarburg haben viele Aktionen von Foodsharing in dieser Zeit unterstützt - der Zusammenhalt und der Wunsch zu helfen war überall zu spüren.
 
Ein neuer Gastronom in unserem Burgrestaurant hat gleich zur ersten Fairteilung dieser Art viel frisches Brot und Süßes für die Kinder gebacken. Unsere lokale Metzgerei Thörnich stellte extra Wurst für unsere Abholgruppe her. In der Osterzeit konnten wir tolle Pakete verteilen. Das Hotel Erasmus aus Trassem hat uns viele Packungen Nudeln geschenkt. Die Firma Britz Lebensmitteltechnik verschenkte über den Saarburger Gewerbeverband 16 Kartons frische Pommes, welche von Restaurants wegen Schließung nicht abgenommen wurden. Die Eisdiele Fattor spendete 60 Portionen Eis - daraufhin haben wir Eismobil gespielt und viele Kinder haben uns freudig empfangen. Auch unsere beiden großen Discounter spendeten für die Pflegekräfte im Krankenhaus und im Altenheim viel Süßes. Eine Firma aus dem Hunsrück hat uns sehr viele Tiefkühlprodukte überlassen - diese konnten wir in Saarburg und in Konz in einer Riesenaktion verteilen (siehe Bild).

Die Empfänger\*Innen waren während der ersten 10 Wochen auf die Leute begrenzt, welche in dieser schweren Zeit dringend Unterstützung benötigten. Nun verteilen wir wieder wie gewohnt, mit dem notwendigen Abstand und dem Tragen von Masken. 

Wir konnten an vielen Stellen unterstützen und das bereitete auch uns viel Freude. 
