---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        

       
---

Kurzbeschreibung von Saarburg-Kell und Konz

- foodsharing Bezirk seit Januar 2020 mit der Stadt Saarburg und Konz, sowie der Verbandsgemeinde Saarburg-Kell (vorher beim Bezirk Trier)
- Anzahl laufende Kooperationen: 16
- Art der Kooperationen: Bäckereien, Supermärkte, Gemüsehändler, Käse-Manufaktur, Tankstelle und Regionalladen
- Besonderes: Im Dezember 2018 hatten wir die erste Abholung in Saarburg mit Unterstützung von FoodsavernInnen aus Trier und Konz. Danach haben wir den Bereich Saarburg aufgebaut, nun uns aus organisatorischen Gründen die Bezirke geteilt. <br>
Wir sind ein netter Ort an der Saar (7500 EinwohnerInnen), touristisch sehr gut erschlossen und flächenmäßig eine größere Verbandsgemeinde mit ländlicher Struktur.


(Stand: Februar 2020)
