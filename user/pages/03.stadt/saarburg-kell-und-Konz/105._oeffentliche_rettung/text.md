---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: false
image_align: right
---

## Öffentliche Lebensmittelrettung

In Saarburg wird derzeit einmal pro Woche die komplette Abholung der Saarburger Betriebe dazu genutzt, die Öffentlichkeit damit zu versorgen.
Eine ehemalige Metzgerei wurde im Oktober 2019 zu neuem Leben erweckt – sie stand viele Jahre leer. 
Das war der 2. Schritt unserer Entstehung in Saarburg, es fing alles mit einem Tisch vor unserer Garage an, mit Verteilungen starteten wir in unserem Vorraum der Kellerräume.
