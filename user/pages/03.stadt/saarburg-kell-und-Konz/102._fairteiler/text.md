---
title: Fairteiler
menu: Fairteiler
published: true
image_align: right
---

## Fairteiler

Wir haben im FS-Bezirk Saarburg-Kell und Konz derzeit zwei aktive Fairteiler. Diese stehen in der Stadt Saarburg und in der Stadt Konz. In Saarburg haben wir ganz aktuell auch die Möglichkeit gekühlte Waren zu verteilen.
