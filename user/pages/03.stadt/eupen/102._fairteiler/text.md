---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler
Es gibt in Eupen drei Fairteiler: einen in der Unterstadt am Viertelhaus Cardijn auf dem Scheiblerplatz (siehe Foto), einen Kühlschrank im Animationszentrum Ephata in der Bergkapellstrasse sowie einen weiteren am alten Schlachthof, Rotenbergplatz 19 am Jugendtreff Xdream. Ein weiterer wird im Laufe des Monats November in Kettenis seinen Platz am Parkplatz der Mehrzweckhalle finden.
Auf einer [Karte]( https://www.google.com/maps/d/viewer?mid=1TggYrFuYQVQ8kiDWtarNV7Ao_1YfwGbf&shorturl=1&ll=50.5600382192918%2C6.122219499999915&z=10
) sind alle Fairteiler Standorte verzeichnet.
