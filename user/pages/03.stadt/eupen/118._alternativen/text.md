---
title: Förderung von Alternativen
menu: alternativen
image_align: right
---

##Förderung von Alternativen

Die Stadt Eupen ist seit 2014 [Fairtrade-Stadt](https://www.eupen.be/leben-in-eupen/engagement-und-initiativen/kampagnen/fairtrade-gemeinde/), wobei zu den Aufgaben der belgienweiten Kampagne auch die Förderung lokaler Produzenten gehört.

Im Rahmen des Kommunalen Naturentwicklungsplans Eupen verwandelten die Einwohner\*innen Eupens zum Beispiel ihren Stadtpark in einen essbaren Wald. Gemeinsam wurden ca. 200 neue Bäume gepflanzt. Mehr Infos und ein Video der Aktion findet ihr unter diesem [Link](https://www.eupen.be/baumpflanzaktion-im-ostpark/). 