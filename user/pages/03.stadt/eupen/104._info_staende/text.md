---
title: Info Stände
menu: info_staende
image_align: right
---

## Info Stände

Beim Fest der Nachhaltigkeit im Mai 2019 haben wir unsere Initiative vorgestellt. 
Während der Europäischen Woche zur Abfallvermeidung hat Foodsharing mit den Organisationen „Miteinander Teilen“, „Landfrauenverband“ und „Die Eiche VoG“ den Fim „ Taste the waste“ gezeigt: am 18. November in Büllingen, am 19. November in Eupen und am 21. November in Sankt Vith. Alle Sekundarschulen wurden eingeladen und rund 100 Schülerinnen und Schüler nehmen an den Schulaufführungen teil. Ein Informationsstand und ein Fairteiler werden bei diesen Abenden aufgebaut.
