---
title: Eupen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.63057
        lng: 6.03132
    status: approved

content:
    items: @self.modular
---
