---
title: Events
menu: events
image_align: left
---

## Events

Die foodsharing Gruppe organisiert im Rahmen der Europäischen Woche zur Abfallreduktion die Filmprojektionen „Taste the Waste“.
In Zusammenarbeit mit der Seminarküche Naturgenuss hat Foodsharing mehrere Kochtreffs organisiert, bei welchen Reste in ein leckeres Essen verwandelt wurden und gemeinsam gegessen wird. 
