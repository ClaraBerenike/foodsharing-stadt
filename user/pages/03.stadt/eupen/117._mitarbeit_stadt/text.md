---
title: Anstellung bei der Stadt
menu: mitarbeit_stadt
image_align: left
---

## Anstellung bei der Stadt
Unsere Kooperationspartnerin von der Stadt ist Alexandra Hilgers.  
So könnt ihr sie kontaktieren:   
Umweltberaterin   
Stadt Eupen, Städtebau- und Umweltdienst   
Am Stadthaus 1  
4700 Eupen  
Tel. direkt: 087/59 58 19  
Email: <alexandra.hilgers@eupen.be>
