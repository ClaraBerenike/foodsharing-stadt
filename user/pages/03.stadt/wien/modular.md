---
title: Wien
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 48.2082
        lng: 16.3725
    status: pending

content:
    items: @self.modular
---
