---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
---

## Öffentlichkeitsarbeit der Stadt


Ein Leitfaden zur Lebensmittelweitergabe der Stadt Wien: 

Tipps und Möglichkeiten der Lebensmittelweitergabe in Wien sind [hier](https://www.wien.gv.at/umweltschutz/nachhaltigkeit/pdf/leitfaden-lebensmittelweitergabe.pdf) zu finden