---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 1700
- AbholerInnen mind. einmal im Monat: ca. 400
- Engagierte über Abholungen hinaus: ca. 40
- Kontakt: <wien@foodsharing.network>