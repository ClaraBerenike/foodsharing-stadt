---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Wien

- foodsharing Bezirk seit 2013
- Anzahl Kooperationen: ca. 170
- Besonderes: 2014 haben wir eine Petition an die Bundesregierung gemacht. Außerdem haben wir einige Preise gewonnen wie Viktualia Award und den respekt.net-
Preis "Orte des Respekts".


(Stand: Dezember 2019)
