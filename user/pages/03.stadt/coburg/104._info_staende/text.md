---
title: Info Stände
menu: info_staende
image_align: right
---

## Info Stände

Das machen wir! Unser Stand auf den Designtagen, welcher in Zusammenarbeit mit Transition gestaltet wurde,
seht ihr auf dem Foto.