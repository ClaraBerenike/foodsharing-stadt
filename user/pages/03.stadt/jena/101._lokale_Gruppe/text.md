---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- FoodsaverInnen: 320
- AbholerInnen mind. einmal im Monat: 150
- Engagierte über Abholungen hinaus: 20
- Kontakt: <foodsharing.jena@posteo.de>
