---
title: Fairteiler
menu: Fairteiler
published: true
image_align: right
---

## Fairteiler

In Jena & Umgebung gibt es insgesamt 6 Fairteiler: am Grünen Haus beim Jenaer Theater, im Frei(t)raum der Friedrich-Schiller-Universität, im Damenviertel (Am Planetarium 41), in der Magdelstube (Magdelstieg 23) und im Nachbarort Stadtroda (Steinweg 6). Seit Neuestem unterstützt uns sogar beim Verteilen der Lebensmittel ein mobiler Fairteiler. Der Fahrradanhänger F-RED zieht regelmäßg seine Runden durch die Stadt, um Menschen mit Essen zu beglücken. Eine Liste der vorhandenen FairTeiler findet ihr unter diesem [Link](https://bit.ly/3dldgdB).

Die Fairteiler werden regelmäßig von den FoodsaverInnen beliefert und gesäubert.
