---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona

Zu Beginn der Corona-Pandemie musste die Jenaer Tafel für zwei Wochen schließen. Wir konnten durch das riesige Engagement unserer foodsaver die Arbeit der Tafel in diesen zwei Wochen fortsetzten. Dabei durften wir Räume von verschiedenen Initiativen wie dem DRK, dem Stadtteilladen Magdelstube, dem Jugendtreff Tacheless und vielen Weiteren für die Sortierung und Verteilung der großem Mengen Lebensmittel nutzen.
Für die Tafelnutzer\*innen wurden fertige Kisten gepackt und täglich, mit der Unterstützung der Südkurve Jena, kontaktfrei übergeben. Die restlichen Lebensmittel konnten dann an verschieden Orten im Jenaer Stadtgebiet täglich verteilt werden.
Es war sehr bewegend, wie in dieser schwierigen Zeit Menschen solidarisch zusammenarbeiten.

Seid der Corona-Pandemie verteilen wir nun regelmäßig öffentlich Lebensmittel wodurch die Aufmerksamkeit auf foodsharing und das Thema Lebensmittelverschwendung sehr gestiegen ist.
