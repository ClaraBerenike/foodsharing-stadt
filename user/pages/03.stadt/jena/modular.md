---
title: Jena
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.92819
        lng: 11.58807
    status: approved

content:
    items: @self.modular
---
