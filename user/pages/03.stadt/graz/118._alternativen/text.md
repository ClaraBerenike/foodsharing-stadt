---
title: Förderung von Alternativen
menu: alternativen
image_align: right
---

##Förderung von Alternativen

Die Plattform "Nachhaltig in Graz" wurde mit dem österreichischen Klimaschutzpreis in der Kategorie „tägliches Leben“ ausgezeichnet. Diese Plattform gibt Menschen in Graz, die nachhaltiger Leben wollen, praktische Tipps und Empfehlungen für bspw. verpackungsfreie Geschäfte, nachhaltige Restaurants, Initiativen und auch zum Thema Literatur und Veranstaltungen. In dem dazugehörigen „Verschenkladen“ in der Leonhardstraße können Kleidungsstücke gespendet, aber auch erworben werden, um Abfall zu vermeiden und Ressourcenschonender leben zu können. 
[Link zur Plattform Nachhaltig in Graz](https://nachhaltig-in-graz.at )


Zusätzlich gibt es von der Stadt Graz die Möglichkeit Gemeinschaftsgärten mit einem einmaligen Gründungsgeld zu fördern, sowie die anfallenden Kosten für die Arbeit im Garten im Laufe des Jahres mit bis zu 800€ pro Gartengruppe zu fördern.

