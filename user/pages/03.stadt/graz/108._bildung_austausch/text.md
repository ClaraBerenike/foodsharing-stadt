---
title: Bildungsangebot
menu: bildungsangebot
image_align: rechts
---

## Bildungsangebot

 Unsere Bildungsangebote werden mit Hilfe von Infoveranstaltungen in der Uni bzw. auf Veranstaltungen wie bspw. Kleidertausch und in Kooperationen mit Tanz- und Musikvereinen zielgruppenübergreifend umgesetzt. 
 
 Derzeit wird eher auf der Informationsebene agiert. Informationsabende werden vor allem auf der Universität Graz organisiert. Aber auch Radiosendungen werden mit Hilfe des Radio Helsinkis produziert, um mediale Aufmerksamkeit für foodsharing und den Umgang mit Lebensmitteln zu bekommen.