---
title: Graz
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 47.07086
        lng: 15.43828
    status: approved

content:
    items: @self.modular
---
