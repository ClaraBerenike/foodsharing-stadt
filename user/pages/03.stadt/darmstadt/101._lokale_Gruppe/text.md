---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 510
- Abholer\*innen mind. einmal im Monat: 140
- Engagierte über Abholungen hinaus: 40
- Kontakt: <darmstadt@foodsharing.network>