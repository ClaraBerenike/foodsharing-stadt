---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Darmstadt
- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 73
- Viele Infos über unser Team und Aktionen findest du auf unserer eigenen [Webseite](https://www.foodsharing-darmstadt.de/).
