---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
image_align: right
---

## Öffentlichkeitsarbeit Team
Für unsere Öffentlichkeitsarbeit haben wir eine eigene AG. 
Schreib uns gerne eine Mail an <oeffentlichkeitsarbeit.darmstadt@foodsharing.network>.
