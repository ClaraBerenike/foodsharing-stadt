---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
---

## Weitere lokale Initiativen

Unzählige Projekte setzen sich in unserer Stadt gegen Verschwendung ein. Zum Beispiel Ingenieure ohne Grenzen,
Repaircafes, verschiedene private und kirchliche Projekte.
Aber auch unverpackt und die "Arche", ein Bioladen.
