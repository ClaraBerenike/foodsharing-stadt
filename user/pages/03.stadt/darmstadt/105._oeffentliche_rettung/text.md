---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
---

## Öffentliche Lebensmittelrettung

Wir haben sowohl auf dem Weihnachtsmarkt als auch auf dem Schlossgrabenfest 2019 Lebensmittel gerettet.
Eine besondere Herausforderung war dabei die Absprache mit der Stadt bzw.
den Betreiber\*innen und genügend Helfer\*innen zu bekommen (Abholungen
sind Nachts).
