---
title: Info Stände
menu: info_staende
image_align: left
---

## Info Stände

Am VHS Erkrath Aktionstag Nachhaltigkeit Sa 22.6.19 gab es einen Foodsharing 
Infostand plus Schnippeln aus geretteten Lebensmitteln. 
[Link zum Event](https://www.wz.de/nrw/kreis-mettmann/erkrath/erkrathernachhaltigkeitsmesse-empfiehlt-verwenden-stattverschwenden_aid-39646925)