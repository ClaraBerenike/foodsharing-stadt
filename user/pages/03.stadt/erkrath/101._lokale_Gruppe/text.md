---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 32, davon viele mit Stammbezirk Düsseldorf
- Abholer\*innen mind. einmal im Monat: in den Nachbarbezirken etwa 20
- Engagierte über Abholungen hinaus: 30
- Kontakt: <erkrath@foodsharing.network>