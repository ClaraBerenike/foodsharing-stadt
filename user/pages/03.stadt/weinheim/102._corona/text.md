---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Während des Lockdown 2020 übernahm foodsharing alle Betriebe der Tafel Weinheim da diese coronabedingt schließen musste.
Es fanden wöchentlich bis zu drei mal ‚Tafelersatz‘-Termine statt. Dabei wurden Lebensmittel an Personen ausgegeben, die diese benötigten.
Die Stadt unterstützte durch Öffentlichkeitsarbeit und organisatorischen Aufgaben.

