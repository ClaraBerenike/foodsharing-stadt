---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot

Der Stadtjugendring thematisiert foodsharing im Rahmen einer Kochgruppe, die mit geretteten Lebensmitteln arbeitet.
