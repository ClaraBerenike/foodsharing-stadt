---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Es gibt seit 2018 eine Arbeitsgruppe zum Thema Öffentlichkeitsarbeit. Verantwortlich sind:
Kira Appelt, Klaudia Freund und Tamy Fraas
