---
title: Weinheim
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.54725
        lng: 8.67086
    status: pending

content:
    items: @self.modular
---
