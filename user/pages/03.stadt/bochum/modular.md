---
title: Bochum
menu: Home

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.48182
        lng: 7.21967
    status: pending

onpage_menu: false
body_classes: 

content:
    items: @self.modular
---
