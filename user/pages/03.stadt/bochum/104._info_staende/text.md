---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Unter dem Titel 'Utopisches Flanieren' hat eine Gruppe von Künstler\*Innen des atelier automatique und Aktivist\*Innen des Netzwerks Stadt für Alle im September 2020 zu einer utopischen Reise durch Bochum eingeladen.
Für das Projekt wurden die Träume von Menschen aus verschiedenen Projekten und Orten in separaten Audiodateien festgehalten. Neben den Eindrücken von Initiativen wie Bodo e.V. und der Rosa Strippe wurden auch die Fantasien einer Foodsaverin über eine 'Straße des Fortschritts' in der Brückstraße auditiv festgehalten (https://utopisches-flanieren.de/tracks).
Für den Audiowalk können Menschen entweder allein oder in einer kleinen Gruppe durch die Stadt flanieren und die QR-Codes, die an verschiedenen Orten angebracht wurden, ablaufen. Der Audiowalk ist eine Übung zum Träumen und soll Lust darauf machen, die Stadt selbst ein Stückchen mitzugestalten. 

Auch in dem Bündnis bochum for future, das verschiedene Klima-Initiativen in Bochum verbindet, sind wir aktiver Bestandteil und haben diverse Veranstaltungen und Demos begleitet und mit Lebensmittelspenden unterstützt.