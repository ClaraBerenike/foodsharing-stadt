---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

foodsharing Bochum steht in engem Kontakt mit Bochum Marketing (kurz BoMa), die unter anderem Stadtfeste und den Weihnachtsmarkt organisieren.
Seit 2019 retten wir auf verschiedenen dieser Feste und Veranstaltungen. Im gleichen Jahr konnten wir auch zum ersten Mal auf dem Weihnachtsmarkt retten. Wie bereits genannt haben wir von BoMa durch das städtische Projekt "Tapetenwechsel" einen Leerstand in der Innenstadt kostenlos zur Verfügung gestellt bekommen und werden auch jetzt für den Fairteiler "Brücki" finanziell unterstützt.

Einige Male haben wir bei dem auf dem Gelände der Fiege Brauerei stattfindenden Food Lovers Market am Ende der Veranstaltungen die nicht mehr verwendbaren Lebensmittel rettet und weiterfairteilt.

Bei der auf dem Gelände der Rotunde stattfindenden Veranstaltung "Weine vor Freude" haben wir im Jahr 2019 einmalig die übrig gebliebenen Lebensmittel gerettet.
