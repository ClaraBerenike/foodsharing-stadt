---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Bochum

- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 34 laufende Kooperationen
- Besonderes: Wir wachsen stetig und organisieren uns in vielen Arbeitsgruppen, sind also auch über das Retten von Lebensmitteln hinaus sehr aktiv.


(Stand: Februar 2021)
