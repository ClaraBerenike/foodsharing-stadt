---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

foodsharing in Bochum konnte während Corona wachsen, obwohl es einige Rückschläge gab. Wir hatten schon zuvor Kontakte zu Ämtern, die uns zeitnah bestätigen konnten, dass wir trotz Beschränkungen weitermachen dürfen. Die Foodsaver\*Innen im Bezirk retten trotz reduzierter Slots und höherer Hygienestandards weiter. Die Einführungen für neue Foodsaver\*Innen konnten nach einer etwa zweimonatigen Pause wieder aufgenommen werden. Auch neue Kooperationen wurden begonnen - teils sogar aufgrund der Pandemie.
Einige Fairteiler mussten vorübergehend leider geschlossen werden und sind bis heute (Februar 2021) noch nicht wieder geöffnet. Allerdings konnten wir auch einen neuen 24/7 Fairteiler eröffnen.
Auch der Umstieg der Plena sowie Arbeitsgruppen-und Vernetzungstreffen ins Digitale hat uns kaum Probleme bereitet. Inzwischen sind viele sicher im Umgang mit den Kommunikationsplattformen Mumble und BigBlueButton.
