---
title: Runder Tisch
menu: runder_tisch
published: true
image_align: left
---

## Runder Tisch

In Bochum gibt es seit September 2020 einen Ernährungsrat (https://ernaehrungsrat-bochum.de).
Im Juni 2019 haben wir für die durch den Ernährungsrat organisierte 'Schnippeldisko' im Rahmen der BoBiennale in Bochum Langendreer Lebensmittel zur Verfügung gestellt. 

Im September 2019 hat eine unserer Foodsaver\*Innen an dem Stadtteilrundgang des Ernährungsrats teilgenommen und dabei den Fairteiler in der Bochumer Ehrenamtsagentur vorgestellt. Der Stadtrundgang fand unter dem Motto „Nachhaltig Ernähren? Aber wie!" statt und gab einen Einblick in Alternativen und Möglichkeiten zu einer nachhaltigen und wohnortnahen Versorgung mit Nahrungsmitteln. 
