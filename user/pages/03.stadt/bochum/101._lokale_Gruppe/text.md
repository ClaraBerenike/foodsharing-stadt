---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Unsere lokale foodsharing Gruppe existiert seit 2014 und wächst seitdem stetig. 229 Foodsaver\*Innen waren 2020 bei Rettungseinsätzen unterwegs. Gut 50 betriebsverantwortliche Personen koordinieren die Abholungen bei den Bochumer Betrieben. Außerdem haben wir 15 Arbeitsgruppen zum Beispiel für Öffentlichkeitsarbeit, Fairteiler oder Koch- und Schnibbelevents. Seit 2019 können die Bochumer Foodsaver\*Innen Mediationsteam, Meldungsteam und Botschafter\*nnen wählen.
