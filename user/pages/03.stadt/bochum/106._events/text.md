---
title: Events
menu: events
published: true
image_align: left
---

## Events

Wir haben eine monatlich stattfindende Schnibbelküche für alle Menschen. Bei dieser kochen wir im Neuland (http://neuland-bochum.de/) mit geretteten Lebensmitteln.
Leider findet diese zur Zeit wegen Corona nicht statt. Außerdem stellt uns der Verein seine Räume für Plena und Austauschtreffen zur Verfügung.
