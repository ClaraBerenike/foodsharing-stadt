---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Siegen

- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 37
- Art der Kooperationen: Supermärkte, Biomärkte, Bäckereien, Wochenmärkte, weitere Lebensmittelgeschäfte, Kino, Mensen, Unverpackt Laden
- Besonderes: Neben einer aktiven Foodsharing-Szene werden in Siegen auch immer mehr Akteure in den Bereichen Gemeinschaftsgärten und -küchen aktiv. Aus der Initiative Siegen isst bunt hat sich 2020 der Verein Lebensmittel Teilen e.V. gegründet. Weitere Informationen unter: http://www.lebensmittelteilen.de/index.html



(Stand: 20.05.21)
