---
title: Siegen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.8766
        lng: 8.0255
    status: pending

content:
    items: @self.modular
---
