---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Trotz oder gerade wegen der Corona-Pandemie ist ein neuer Fairteiler beim Heimatverein Achenbach entstanden. Dieser wird durch eine Telegram-Gruppe unterstützt. Der Fairteiler steht in einem Reallabor. Hierdurch können Studierende der Universität Siegen untersuchen, wie die Praktiken rund um den Fairteiler unterstützt werden können (z.B. durch einen Gemeinschaftsgarten in unmittelbarer Nähe). Andere Fairteiler, die in Uni-Gebäuden stehen, sind aufgrund der Hochschulschließungen leider zurzeit inaktiv.
