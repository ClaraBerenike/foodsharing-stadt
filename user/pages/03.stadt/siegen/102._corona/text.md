---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

Jeden Dienstag und Samstag gibt es eine Lebensmittel-Fairteilung vor dem Sozialkaufhaus des Heimatvereins Achenbach in Siegen. Dabei ist jede\*r herzlich zur Fairteilung eingeladen. Neben der Rettung von Lebensmitteln steht vor allem das Teilen mit einer Gemeinschaft im Fokus des Projektes. Hierzu dienen, neben den wöchentlichen Fairteilungen, ein Gemeinschaftsgarten sowie das fairteilen anderer Lebensmittel-Ressourcen wie Saatgut, Erde, Wissen usw. Das Projekt wurde im Februar 2020 vom Land NRW mit dem Engagementpreis ausgezeichnet.
Um den aktuellen Hygieneanforderungen zu entsprechen, haben wir Ordner aufgestellt, die auf den Abstand der Menschen zueinander sowie auf das Tragen von Masken achten.  Seit dem ersten Lockdown führen wir in unregelmäßigen Abständen Ersatzabholungen für die Tafeln durch. Auch haben wir mehrere Großabholungen, die aufgrund des Lockdowns notwendig wurden, gestemmt. Abgeholt haben wir z.B. bei einem Catering-Service, der Uni-Mensa, einem Möbelhaus und einem Kino.
