---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: left
---

## Lebensmittelanbau

Angefangen haben wir 2020 mit drei Hochbeeten am KIQ (KulturIntegrationQuartier). Mittlerweile gärtnern wie in zwei weiteren Gemeinschaftsgärten: beim Heimatverein Achenbach und beim Lebendigen Haus (Häusling). Auch haben wir bereits zwei Saatgutfestivals organisiert. Aufgrund der Umstände, die Corona mit sich brachte, haben wir außerdem eine dezentrale Gemüseanbau-Aktion ins Leben gerufen. Sie trägt den wundervollen Namen Gemüse sucht ein Zuhause. Kontaktiert uns für weitere Informationen gern per Mail an info@siegenisstbunt.de oder folgt Siegen isst bunt bei Facebook und Instagram

