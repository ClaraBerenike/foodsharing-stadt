---
title: Events
menu: events
published: true
image_align: left
---

## Events

Foodsharing-Events können z.B. Kochabende und Saatgutbörsen einschließen. Diese dienen dazu, Menschen in Kontakt mit der Thematik rund um einen nachhaltigen Umgang mit Lebensmitteln zu bringen und eine Gemeinschaft aufzubauen. 
Bei Kochabenden werden beispielsweise primär gerettete Lebensmittel in Gemeinschaft verkocht und Themen rund um Lebensmittelverschwendung und gesunde Ernährung behandelt.
Aufgrund der aktuellen Situation ist es zurzeit nicht möglich diese Treffen in Präsenz zu veranstalten, jedoch ist ein digitales Format vorstellbar. 
