---
title: 'foodsharing Städte'
process:
  twig: true

cities:

content:
    items: '@self.children'
---

## Auf dem Weg zur foodsharing Stadt

Viele Menschen in zahlreichen Städten engagieren sich bereits über [foodsharing.de](https://foodsharing.de/) als Foodsaverinnen und retten Lebensmittel vor dem Müll. Die Bewegung foodsharing-Städte möchte eine Plattform bieten, um Aktivitäten, die über das Lebensmittelretten hinaus gehen darzustellen, und die Zusammenarbeit mit lokalpolitischen Akteurinnen fördern.

Teilnehmende Städte unterscheiden sich dahingehend, ob sich diese noch "auf dem Weg" befinden oder bereits als "foodsharing-Stadt" ausgezeichnet wurden. Letzteres ist der Fall, wenn es neben einem foodsharing-Team vor Ort auch eine von der Stadtverwaltung unterschriebene Motivationserklärung als Symbol der Partnerschaft zwischen foodsharing und der öffentlichen Hand gibt.


{{ mapcities() }}



<style>
{
  box-sizing: border-box;
}

.column {
  float: left;
  width: 50%;
  padding: 10px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
  }
}
</style>



<div class="row">
  <div class="column">
    <h3>Städte auf dem Weg</h2>
    <p>
<ul>
<li><a href="/stadt/bad-camberg">Bad Camberg</a></li>
<li><a href="/stadt/bayreuth">Bayreuth</a></li>
<li><a href="/stadt/bochum">Bochum</a></li>
<li><a href="/stadt/braunschweig">Braunschweig</a></li>
<li><a href="/stadt/coburg">Coburg</a></li>
<li><a href="/stadt/darmstadt">Darmstadt</a></li>
<li><a href="/stadt/erkrath">Erkrath</a></li>
<li><a href="/stadt/hamburg">Hamburg</a></li>
<li><a href="/stadt/hof">Hof</a></li>
<li><a href="/stadt/lueneburg">Lüneburg</a></li>
<li><a href="/stadt/mainz">Mainz</a></li>
<li><a href="/stadt/mettmann">Mettmann</a></li>
<li><a href="/stadt/rostock">Rostock</a></li>
<li><a href="/stadt/saarburg-kell-und-konz">Saarburg-Kell und Konz</a></li>
<li><a href="/stadt/siegen">Siegen</a></li>
<li><a href="/stadt/stuttgart">Stuttgart</a></li>
<li><a href="/stadt/trier">Trier</a></li>
<li><a href="/stadt/wien">Wien</a></li>
<li><a href="/stadt/wurzen">Wurzen</a></li>
</ul>   


    </p>
  </div>

  <div class="column">
    <h3>foodsharing Städte</h2>
    <p>    
<ul>
<li><a href="/stadt/diez">Diez</a></li>
<li><a href="/stadt/eupen">Eupen</a></li>
<li><a href="/stadt/graz">Graz</a></li>
<li><a href="/stadt/jena">Jena</a></li>
<li><a href="/stadt/recklinghausen">Recklinghausen</a></li>
<li><a href="/stadt/remscheid">Remscheid</a></li>
<li><a href="/stadt/st-ingbert">St. Ingbert</a></li>
<li><a href="/stadt/weinheim">Weinheim</a></li>

</ul>  
    
    
    </p>
  </div>
</div> 


