---
title: Events
menu: events
image_align: right
---

## Events

Die Schnippeldisco ist unser bekanntestes Event, um auf Lebensmittelverschwendung aufmerksam zu machen. In Kooperation mit Slow Food, dem Stadtgarten Bebelhof und Transition Town Braunschweig findet das öffentliche Kochevent seit 2018 auf dem Anna-Amalia-Platz hinter dem Schloß statt.

