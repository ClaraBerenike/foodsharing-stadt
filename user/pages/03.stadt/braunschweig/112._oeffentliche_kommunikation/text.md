---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
---

## Öffentlichkeitsarbeit der Stadt

2017 gewannen wir als Initiative foodsharing Braunschweig den [Klimaschutzpreis der Stadt Braunschweig](https://www.braunschweig.de/leben/umwelt_naturschutz/klima/klimaschutzpreis/2017/sonderkategorie.php#foodsharing). Mit Freude nahmen wir die Anerkennung war. Das Preisgeld ging an der überregional aktiven foodsharing e.V..

