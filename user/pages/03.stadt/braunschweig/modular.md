---
title: Braunschweig

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 52.26468
        lng: 10.52378
    status: pending

menu: Home
onpage_menu: false
body_classes:

content:
    items: @self.modular
---
