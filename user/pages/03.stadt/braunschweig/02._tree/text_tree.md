---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png          
    
---

Kurzbeschreibung von Braunschweig
- foodsharing Bezirk seit März 2014
- Anzahl Kooperationen: 57
- Art der Kooperationen: 9 Bäcker, 10 Supermärkte, 8 Marktbetriebe, 10 Cateringbetriebe, 4 Reformhausbetriebe, 1 Hof, 3 Cafés, 3 Restaurants
- Besonderes: Braunschweig ist keine klassische Unistadt. Trotzdem sind die meisten Foodsaver Studierende. Wir freuen uns über zunehmendes Interesse aus anderen Altersgruppen. Unser Bezirk ist gut vernetzt mit unseren Nachbarn Wolfenbüttel, Wolfsburg, Gifhorn, Lehrte und Peine.


(Stand: Dezember 2019)
