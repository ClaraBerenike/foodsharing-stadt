---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 804
- Abholer\*innen mind. einmal im Monat: ca. 140
- Engagierte über Abholungen hinaus: 30
- Kontakt: <braunschweig@foodsharing.network>