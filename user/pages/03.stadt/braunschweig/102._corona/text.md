---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona

In Braunschweig ist foodsharing weiterhin sehr aktiv. Leider mussten wir zwar unseren Fairteiler vorübergehend schließen, da die Uni-Gebäude nicht mehr zugänglich sind und es außerdem regelmäßig zu Menschenansammlungen kam. Dafür setzten wir uns aber zusammen mit einigen anderen Organisationen dafür ein, dass ein Teil der geretteten Lebensmittel an Bedürftige geht. Unter anderem beliefern wir eine Obdachlosenhilfe, eine Unterkunft für Obdachlose und die Frauen in der Braunschweiger Bordellstraße, die momentan nicht arbeiten können. Da die Braunschweiger Tafel momentan nur eingeschränkt arbeitet, holen wir bei mehreren Supermärkten als vorübergehender Tafelersatz ab. Die dort geretteten Lebensmittel werden ausschließlich an Bedürftige weitergegeben. Bei unseren Abholungen berücksichtigen wir natürlich die Sicherheitsmaßnahmen, sodass bei keiner Abholung mehr als zwei Personen anwesend sind, Masken und Handschuhe getragen und entsprechend Abstand gehalten wird. Alle ein bis zwei Wochen treffen wir uns zum Orga-mumble, um möglichst schnell auf neue Entwicklungen reagieren zu können. Trotz aller Einschränkungen läuft es also bei uns sehr gut und wir konnten sogar neue langfristige Kooperationen hinzugewinnen und den Kontakt zur Tafel verbessern.