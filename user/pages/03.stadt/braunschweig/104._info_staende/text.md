---
title: Info Stände
menu: info_staende
image_align: right
---

## Info Stände

Regelmäßig werden Info-Stände veranstaltet, um die foodsharing Idee weiter zu verbreiten. So waren wir auf dem Genussmarkt von Slow Food vertreten, sowie bei allen Umsonstflohmärkten von Transition Town und dem AStA der TU Braunschweig. An der Universität sind wir ebenfalls öfter mit einem Infostand, insbesondere um unseren Fairteiler zu bewerben.