---
title: Münster
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.96257
        lng: 7.62540
    status: pending

content:
    items: @self.modular
---
