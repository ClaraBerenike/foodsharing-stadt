---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 100 
- AbholerInnen mind. einmal im Monat:  50
- Engagierte über Abholungen hinaus:   30
- Kontakt:   ...@foodsharing.network
