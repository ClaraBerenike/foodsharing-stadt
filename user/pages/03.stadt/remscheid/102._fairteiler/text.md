---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Wir haben über Remscheid verteilt ca.
50 private Fairteilergruppen, die zumeist
über whatsapp organisiert sind. Dazu
noch öffentliche Verteilungen, die wir
über facebook oder Flyer ankündigen.
