---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Den Bezirk Remscheid gibt es seit September 2016.
- Aktuell 219 angemeldete und davon 100 aktive foodsaver\*innen. Wir haben alles dabei von Studierenden über Hausfrauen
bis zur Rentnerin
- Laufende Kooperationen: 48 darunter auch eine enge Kooperation mit der Tafel, für die wir während der Weihnachtsferien viele Vertretungen fahren dürfen.
Wir können in Remscheid sehr spontan auf Hilfegesuche von Betrieben reagieren und sind meist spätestens innerhalb von einer Stunde an den
Betrieben, wenn akut größere Mengen oder TK-Ware gerettet werden soll.
Dieses Angebot wird häufig und gerne angenommen.
- mindestens 30 foodsaver\*innen engagieren sich übers Retten hinaus bei Infoveranstaltungen oder Bildungsarbeit. Sowohl unter uns foodsaver\*innen als auch zu den Betrieben herrscht ein sehr gutes Klima. Bei unserem Großhändler werden wir nach einer Rettung oft noch auf einen gemeinsamen Tee eingeladen und mindestens einmal im Jahr gibt es ein foodsharing-Fest von und für die
„Mitarbeiter\*innen“, oft auf dem Hof der Botschafterin Elisabeth Erbe.
