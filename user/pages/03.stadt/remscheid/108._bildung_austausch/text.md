---
title: Bildungsangebot
menu: bildungsangebot
published: false
image_align: right
---

## Bildungsangebot

In Kooperation mit dem Büro für kulturelle Bildung der Stadt Remscheid hielten wir Vortrag über foodsharing im ErnstMoritz-Arndt-Gymnasium. Bei einem 2. Termin wurde mit den Schüler\*innen Marmelade gekocht.
Außerdem gab es einen gemeinsamen Vortrag von Tafel und foodsharing zum Thema Nachhaltigkeit
und Lebensmittelverschwendung am Röntgen-Gymnasium.