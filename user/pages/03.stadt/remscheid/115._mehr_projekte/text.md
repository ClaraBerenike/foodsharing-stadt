---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: left
---

## Weitere lokale Initiativen
Zusammen mit einem bekanntem Gastronom (Kai Paulus) kochen wir mit geretteten Lebensmitteln im Café der
Obdachlosenhilfe der Caritas ein 3Gänge-Menü-eine wunderschöne Veranstaltung.
Beide „Betriebe“ liegen in direkter Nachbarschaft und so ergab sich eine wunderschöne Kooperation von Caritas, Villa Paulus und foodsharing.
Wir haben zusammen gekocht und nachher an einer langen
Tafel gemeinsam diniert. foodsharing
verbindet Arm+Reich!
Außerdem gibt es eine jährliche Weihnachtsfairteilung an Heiligabend und 1.Weihnachtstag
(Obdachlosenhilfe, Männerwohnheim
und der Rest öffentlich).

