---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe
Unsere AG Foodsharing-Stadt Recklinghausen hat sich konzipiert, um im Rahmen der Bewegung Maßnahmen zu intensivieren, die über das Retten hinausgehen. Ihr seht uns im rechten Teil des Bildes. Wir freuen uns sehr über die enge Zusammenarbeit mit der Stadtverwaltung Recklinghausen, die
das Thema Lebensmittelwertschätzung tatkräftig vorantreibt.
Unsere Ansprechpartnerinnen von der Stadt Recklinghausen aus der Stabsstelle Klimaschutzmanagement sind Frau Lara Wahrmann (Kontakt: lara.wahrmann@recklinghausen.de) und Frau Lena Germscheid (Kontakt: lena.germscheid@recklinghausen.de). Sie sind im rechten Teil des Bildes zu sehen (Lara Wahrmann links und Lena Germscheid rechts).

In Recklinghausen gibt es zudem 
- Aktive foodsaver\*innen: ca. 40
- Aktive Mitstreiter\*innen über das Lebensmittelretten hinaus: ca. 8-10 Leute  
