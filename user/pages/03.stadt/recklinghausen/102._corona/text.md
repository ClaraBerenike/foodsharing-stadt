---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: right
---

## foodsharing in Zeiten von Corona
Um die Hygienevorschriften einhalten zu können, haben wir einen Gabenzaun eingerichtet. Wir haben das Sortiment erweitert auf Alltags- und Hygieneartikel und die Bürger\*innen aufgefordert, uns mit ihren Spenden zu unterstützen.
Um foodsaver\*innen hinsichtlich Hygiene zu sensibilisieren, wurden Hinweisschilder am Fairteiler bzgl. Hygiene und Abstand angebracht. Gruppentreffen wurden abgesagt oder nach draußen verlegt und die Anzahl der Abholer\*innen wurde reduziert. 



