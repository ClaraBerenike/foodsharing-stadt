---
title: Recklinghausen
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.61451
        lng: 7.19800
    status: pending

content:
    items: @self.modular
---
