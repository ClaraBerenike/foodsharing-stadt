---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events
Gemeinsam mit den kommunalen Servicebetrieben und der Abteilung Klimamanagement der Stadt Recklinghausen haben wir zur europäischen Woche der Abfallvermeidung den Schwerpunkt auf Lebensmittelmüll gelegt. Dazu haben wir gemeinsam mit den städtischen Partner\*innen eine Schaufensteraktion in der Innenstadt organisiert.
