---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: right
---

## Bildungsangebot
Der Träger der Maßnahme „Zukunftswerkstatt“ ist das Bildungszentrum des Handels Recklinghausen. Eine weiterführende Kooperation mit dem Bildungszentrum steht in den Startlöchern. Wir erläutern in einem neuen Projekt mit jungen Erwachsenen, was Foodsharing ist und im Anschluss ist geplant, dass die Projektteilnehmer*innen auch selbst Lebensmittel retten können. 
