---
title: Runder Tisch
menu: runder_tisch
published: true
image_align: left
---

## Runder Tisch
In Absprache mit den städtischen Verwaltungsbereichen Klimaschutzmanagement und KSR (kommunale Servicebetriebe) ist ein runder Tisch Lebensmittelwertschätzung mit weiteren Akteur\*innen aus der Kommune geplant. Pandemiebedingt mussten die Treffen abgesagt werden.