---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: left
---

## Lebensmittelanbau

Seit 2012 ist Recklinghausen fairtrade Stadt. In mehreren urban gardening Projekten engagieren Bürger*innen sich für mehr Grün und Anbau von Gemüse in der Stadt. Ein Unverpackt-Laden sowie drei Bioläden und ein Hofbioladen erweitern das Lebensmittelangebot in der Stadt. „Recklinghausen blüht“ ist eine Bürgerinitiative, die sich für mehr Insektenschutz einsetzt. Die Lokale Agenda 21 ist zudem eine Recklinghäuser Fairtrade- und Nachhaltigkeitsinitiative, von der wir viel Unterstützung erfahren.


