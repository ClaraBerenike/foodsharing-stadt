---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Es gibt eine Betriebsgruppe um die (Groß-)Abholungen bei den vielen Stadtfesten zu organisieren - zuletzt beim Weihnachtsmarkt.
