---
title: Mainz
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.00049
        lng: 8.27484
    status: pending

content:
    items: @self.modular
---
