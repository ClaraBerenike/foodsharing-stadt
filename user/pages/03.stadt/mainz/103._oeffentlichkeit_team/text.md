---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Die Öffentlichkeitsarbeitsgruppe in Mainz ist eher offen und lose in ihrer Zusammenstellung, aber das liegt auch daran, dass es so viele engagierte Menschen in der Gruppe gibt, die sich bei verschiedenen und zahlreichen Projekten einbringen. Wir haben die Öffentlichkeitsarbeitsgruppe in Form eines Betriebs angelegt und es sind ca. 100 Foodsaver\*innen und davon etwa 30 Aktive in diesem Betrieb. Für das Voranbringen von Mainz als foodsharing Stadt haben wir auch noch eine Politik-Arbeitsgruppe.
