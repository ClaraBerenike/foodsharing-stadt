---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
published: true
image_align: left
---

## Öffentlichkeitsarbeit der Stadt

Die Stadt Mainz hat einen [nachhaltigen Stadtplan](https://www.mainz.de/verwaltung-und-politik/verwaltungsorganisation/pressemeldung.php?showpm=true&pmurl=https://www.mainz.de/newsdesk/publications/Mainz/181010100000209604.php) herausgegeben. Die Standorte der Fairteiler wurden auch darin aufgenommen.

