---
title: Runder Tisch
menu: runder_tisch
published: true
image_align: left
---

## Runder Tisch

Erste Schritte zur Gründung eines Ernährungsrats wurden im Frühjahr 2020 angegangen. Es gab zwei Video-Konferenzen, um Menschen aus verschieden Bewegungen, Organisationen und Parteien zu vernetzen. Zurzeit gibt es eine gemeinsame Mailing-Liste, aber die angedachten Aktivitäten wurden verschoben auf Grund der Kontaktbeschränkungen im Zusammenhang mit Covid19.   