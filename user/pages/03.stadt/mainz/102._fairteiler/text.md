---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Es gibt mittlerweile für fast jeden Stadtteil in Mainz einen eigenen Fairteiler. 
- [Altstadt](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=372 )
- [Bretzenheim](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1301 )
- [Hechtsheim](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1770 )
- [Kostheim](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1322 )
- [Mombach](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=799 )
- [Neustadt](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1731 )
- [Oberstadt](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1641 )
- [Weisenau](https://foodsharing.de/?page=fairteiler&bid=46&sub=ft&id=1732 )

Außerdem gibt es WhatsApp- und/oder Telegramgruppen in den Stadtteilen für die Weiterverteilung der Lebensmittel. 
Zudem gibt es eine [Facebookgruppe für Mainz und Umgebung](https://www.facebook.com/groups/FoodsharingMainz)

