---
title: Lebensmittelanbau
menu: lebensmittelanbau
published: true
image_align: left
---

## Lebensmittelanbau

Seit 2015 gibt es eine [solidarische Landwirtschaft in Mainz](https://solawi-mainz.de). Im Laufe der Zeit konnte die Fläche erweitert werden und mittlerweile zählt der Verein über 300 Menschen.

Außerdem gibt es die [Marktschwärmerei in Mainz-Marienborn](https://marktschwaermer.de/de-DE/assemblies/9551/), die den Menschen, die wissen wollen, woher ihre Lebensmittel kommen, mit regionalen Erzeugern zusammenbringt. Geboten wird eine Vielfalt an guten, qualitativ hochwertigen Lebensmitteln aus der Region. 


