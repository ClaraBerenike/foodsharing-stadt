---
title: Hamburg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 53.55071
        lng: 9.99314
    status: pending

content:
    items: @self.modular
---
