---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Hamburg

- foodsharing Bezirk seit November 2013
- Anzahl Kooperationen: Wir haben aktuell 237 laufende Kooperationen in Hamburg.
- Besonderes: Von A bis Z ist alles dabei. Wir haben viele Bäckereien und Cafés, Supermärkte, Wochenmärkte, Drogeriemärkte, Kantinen, Caterings, Kindergärten, Restaurants uvm.


(Stand: 02.02.2020)
