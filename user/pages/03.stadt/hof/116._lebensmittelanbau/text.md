---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
---

## Lebensmittelanbau

Andere Projekte in unserer Umgebung, die sich für mehr Lebensmittelwertschätzung 
einsetzen, findet ihr
[beim Urban Gardening Projekt von awalla](https://awalla.org/urban-gardening/) und 
[bei Urban Gardening Projekten vom Bund Naturschutz Hof](https://kreisgruppehof.bund-naturschutz.com/urban-gardening).



