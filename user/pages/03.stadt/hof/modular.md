---
title: Hof
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.32170
        lng: 11.91790
    status: pending

content:
    items: @self.modular
---
