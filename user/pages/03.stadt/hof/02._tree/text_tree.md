---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Hof
- foodsharing Bezirk seit April 2017
- Anzahl Kooperationen: 10
- Art: Bäckereien, Imbisskette, Café, die örtliche Tafel, Catering Betrieb, Manufaktur für Kantinenessen, Kindergartenkantine
- Besonderes: Wir sind gerade im letzten Jahr sehr gut herangewachsen und freuen uns, in unserem eher ländlichen Gebiet so viele Kooperationen für foodsharing gewonnen zu haben. Wir sind bestrebt, unseren Bezirk weiter aufzubauen und freuen uns über die gute Resonanz zu unseren Fair-Teilern. Zudem geben wir regelmäßig gerettete Lebensmittel beim örtlichen Übernachtungsheim für Obdachlose ab. 


(Stand: Dezember 2019)
