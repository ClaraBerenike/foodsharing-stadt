---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver\*innen: 162
- Abholer\*innen mind. einmal im Monat: ca. 45
- Engagierte über Abholungen hinaus: ca. 5- 10