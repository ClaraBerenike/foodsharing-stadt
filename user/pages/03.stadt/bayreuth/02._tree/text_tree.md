---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        

         
    
---

Kurzbeschreibung von Bayreuth
- foodsharing Bezirk seit April 2014
- Anzahl Kooperationen: 18
- Art: Biobio, 3 Edekamärkte, Senor Taco, türkischer Supermarkt, Cafe, Foe,
Denns, Backwirtschaft, sowie 5 anonyme Betriebe und 5 mit unregelmäßigen
Abholungen auf Zuruf


(Stand: Dezember 2019)
