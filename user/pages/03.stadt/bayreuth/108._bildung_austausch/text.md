---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
---

## Bildungsangebot

Wir haben unsere Fühler nun erstmalig in Richtung Bildungsarbeit
ausgestreckt und durften Schülern einer siebten Klasse einen ganzen
Vormittag das Thema Lebensmittelverschwendung näher bringen. Das hat so
viel Spaß gemacht und wir haben tatsächlich das Gefühl gehabt bei den
Kindern (und Lehrern) etwas bewegt zu haben.