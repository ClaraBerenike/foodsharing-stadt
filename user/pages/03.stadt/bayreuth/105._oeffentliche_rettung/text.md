---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: left
---

## Öffentliche Lebensmittelrettung

Wir holen beim Foodtruckfestival im Herbst und dem Weihnachtsmarkt ab und
haben eine flexible Gruppe für kurzfristige Abholungen.
