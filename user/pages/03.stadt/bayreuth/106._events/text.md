---
title: Events
menu: events
image_align: right
---

## Events

Wir haben ein monatliches Foodsavertreffen (der letzte Dienstag im Monat, 20
Uhr) sowie ein Neulingstreffen nach Bedarf (ca. zweimal im Jahr). 
Am ersten Sonntag im Monat gibt es einen Brunch.
