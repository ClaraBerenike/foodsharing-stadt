---
title: foodsharing in Zeiten von Corona
menu: corona
published: true
image_align: left
---

## foodsharing in Zeiten von Corona

In der Corona-Krise hat sich gezeigt, wie wertvoll die bestehende Struktur für die Stadt Diez ist. Eine Nachbarschaftshilfe wurde ins Leben gerufen und Lebensmittel wurden an kranke Menschen, Corona-Patient\*innen und Menschen in Quarantäne verteilt. Gleichzeitig wurden einmal wöchentlich Lebensmittel in Tüten gepackt und kontaktlos übergeben. 

Außerdem werden 1x wöchentlich gerettete Lebensmittel zu Senior\*innen und Kranken gebracht. Dieses soziale Engagement sorgt gleichzeitig regelmäßig für eine gute Kommunikation des Themas ,,Lebensmittelretten'' in der Öffentlichkeit.
