---
title: Diez
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.36881
        lng: 8.02101
    status: approved

content:
    items: @self.modular
---
