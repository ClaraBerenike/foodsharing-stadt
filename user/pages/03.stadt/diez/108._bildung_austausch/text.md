---
title: Bildungsangebot
menu: bildungsangebot
published: true
image_align: left
---

## Bildungsangebot

Im Willkommenskreis Diez liegt der Fokus vor allem darauf, dass aus Flüchtlingen Ehrenamtliche werden. So verschiebt sich die gesellschaftliche Wahrnehmung, denn Flüchtlinge fairteilen die Lebensmittel. Gleichzeitig wird vor Ort bei den Ehrenamtlichen und bei den Besuchern kommuniziert, was MHD und Verbrauchsdatum bedeuten und wie man damit verfahren kann und wie man Lebensmittelverschwendung vermeiden kann. 

Geplant ist die Zusammenarbeit mit der Aktionsgemeinschaft „Blühende Lebensräume“, die sich um mehr Blüten und Artenvielfalt in und um Diez kümmern, u.a. durch die Anlage von Blühstreifen und Blühbrachen oder durch die Anlage eines verwilderten städtischen Gartens in einen Genussgarten für Tiere und Menschen. Angedacht sind hier z. B. regelmäßige Besuche in Schulen und Kindergärten. Hier soll informiert und aktiv gehandelt werden.

Koordinatorin des Willkommenskreises Diez, Christiane Beule und Bürgermeisterin Annette Wick retten Lebensmittel. Hier findet ein reger Austausch und Ideenfindung in Zusammenhang mit der Vermeidung von Lebensmittelverschwendung statt. 

