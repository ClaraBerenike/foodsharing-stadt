---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Es gibt den Bezirk Limburg, Diez und Umgebung mit 315 Foodsavern, 4 BotschafterInnen, 201 Betrieben und 49 laufenden Kooperationen. 
Stand Juli 2020 wurden bei 13452 Rettungseinsätzen 141.883 kg Lebensmittel gerettet.

Kontakt zu Foodsharing in Diez: Rebecca Lefèvre: 0176 20032377, Anne Olscheski: 0176 60828762

Vertretung der Stadt: Astrid Schröter, Wilhelmstraße 63, 65582 Diez, 06432 95432-0, info@stadt-diez.de


