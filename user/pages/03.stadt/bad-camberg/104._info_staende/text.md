---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Wir arbeiten oft mit dem Klimabündnis Bad Camberg zusammen. So waren wir bereits bei ihrer Demo am 29.11.19 mit einem foodsharing-Stand vertreten. Des Weiteren mit einem Infostand beim Wochenmarkt.