---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

Es gibt den Bezirk Limburg, Diez und Umgebung mit 303 Foodsavern, 4 BotschafterInnen, 205 Betrieben und 50 laufenden Kooperationen. Stand Februar 2021 wurden bei 17.349 Rettungseinsätzen 185.953 kg Lebensmittel gerettet.
