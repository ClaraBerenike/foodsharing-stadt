---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Bad Camberg

- foodsharing Bezirk seit 2017
- Anzahl Kooperationen: 205 Betriebe, 50 laufende Kooperationen
- Besonderes: Wir gehören zum foodsharing-Bezirk Limburg, Diez und Umgebung.
Zurzeit kooperieren wir mit einigen Unternehmen und auch mit dem Lädchen, der ortsansässigen „Tafel“. 
In Bad Camberg sind wir auch gesellschaftlich bereits aktiv. Wir arbeiten mit Kindergärten und Schulen zusammen, haben ein foodsharing-Regal in der Residenz Goldener Grund und konnten bereits mit dem [Klimabündnis Bad Camberg](https://klimabuendnis-bad-camberg.de) zusammenarbeiten. 


(Stand: Februar 2021)
