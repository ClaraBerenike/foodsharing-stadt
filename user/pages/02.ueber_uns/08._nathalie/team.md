---
title: Nathalie
published: true
image_align: left
---


### Nathalie
**Aktiv in:** foodsharing Überregional  
**Foodsaver\*in seit:** Oktober 2015  
**Im Team bin ich** für die Website zuständig. Mir ist besonders wichtig, ein gutes Umfeld zum Lernen zu schaffen, um zusammen unsere technischen Fähigkeiten zu verbessern.  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Ist das eine Fangfrage? Tatsächlich finde ich aber gerettete Bananen super (besonders als Eis!)  
**Die foodsharing Stadt bedeutet für mich:** Ganzheitlich zu denken, Nachhaltigkeit und Gemeinschaft zum Standard zu machen  