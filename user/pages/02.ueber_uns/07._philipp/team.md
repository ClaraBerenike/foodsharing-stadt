---
title: Philipp
published: true
image_align: left
---


### Philipp
**Aktiv in:** theoretisch ganz foodsharing-Land, in überregionalen AG´s, Schwerpunkt Hygiene, KAM für drei Supermarktketten, ohne Pandemie auch aktiv als Retter in Berlin tätig (:  
**Foodsaver\*in seit:** Mai 2017  
**Ich beantworte am liebsten** Nachrichten bei Social-Media.  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Alles, was sich mit Pasta verbinden lässt (:  
**Eine foodsharing Stadt bedeutet für mich:** dass die Kommune, bzw. die Stadt Flagge zeigt gegen Lebensmittelverschwendung und einen wichtigen Schritt Richtung Klimaneutralität geht.  
