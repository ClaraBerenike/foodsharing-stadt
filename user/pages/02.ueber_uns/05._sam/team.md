---
title: Sam
published: true
image_align: left
---


### Sam
**Aktiv im:** Internet  
**Foodsaver\*in seit:** 2017  
**In der Bewegung arbeite ich am liebsten an:** Analoge Gestaltung von Grafiken und Illustrationen  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Wäre schon froh, wenn man hier in der Pampa irgendwas retten könnte...  
**Die foodsharing Stadt bedeutet für mich:** Ein realer Weg in eine Zukunft ökologischen Einklangs (vor allem innerhalb der Städte). Die Möglichkeit zur effektiveren Verwirklichung gemeinnütziger Ideen und Konzepte im Bereich Ernährung und Freiräume.  
