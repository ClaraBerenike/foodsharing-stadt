---
title: Manon
published: true
image_align: left
---


### Manon
**Aktiv im:** foodsharing Städte Team  
**Foodsaver\*in seit:** Herbst 2015  
**Im Team bin ich für** die Begleitung der Teilnehmenden verantwortlich. Ich helfe Städten, Teil der Bewegung zu werden und begleite sie bei ihrem Prozess, eine Stadt mit mehr Lebensmittelwertschätzung zu werden. Zum Beispiel mit Infoveranstaltungen oder Themenworkshops. Außerdem kümmere ich mich um unser inhaltliches Konzept.  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Striezelteig - eine super Grundlage für allerlei Hefegebäck.  
**foodsharing-Städte bedeutet für mich:** Eine Bewegung, die uns zeigt, dass wir Veränderung bewirken können.   
