---
title: "Ideenkatalog"
imports: 'projects.yaml'

---


### Ideenkatalog

In diesem Katalog findet ihr Ideen, wie ihr eure Stadt zu einem Ort
der Lebensmittelwertschätzung macht. Jede Stadt ist anders und deshalb
könnt ihr frei wählen, welche Ideen ihr wann umsetzen
möchtet.


Nur eine Idee ist besonders herausgestellt: unsere Kriterien, um sich foodsharing-Stadt zu nennen.


<!-- projects are included from projects.yaml, template from home.html.twig, style from gallery.css -->
